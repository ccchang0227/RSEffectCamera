package com.realtouchapp.effectcamera;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView tv_main_version = (TextView) findViewById(R.id.tv_main_version);
        PackageInfo pinfo;
        try {
            pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            tv_main_version.setText("v"+pinfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        Button btn_native_camera = (Button) findViewById(R.id.btn_native_camera);
        btn_native_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.startActivity(new Intent(MainActivity.this, NativeCameraActivity.class));
            }
        });

        Button btn_rs_camera = (Button) findViewById(R.id.btn_rs_camera);
        btn_rs_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.startActivity(new Intent(MainActivity.this, RSCameraActivity.class));
            }
        });

        Button btn_gpu_camera = (Button) findViewById(R.id.btn_gpu_camera);
        btn_gpu_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.startActivity(new Intent(MainActivity.this, GPUCameraActivity.class));
            }
        });

    }

}
