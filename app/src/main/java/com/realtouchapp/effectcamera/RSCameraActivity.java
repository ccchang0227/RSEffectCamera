package com.realtouchapp.effectcamera;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.renderscript.Allocation;
import android.renderscript.Matrix4f;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicColorMatrix;
import android.renderscript.ScriptIntrinsicConvolve3x3;
import android.util.DisplayMetrics;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.realtouch.camera.renderscript.RSCameraView;
import com.realtouch.image.rs.ScriptC_Bright;
import com.realtouch.image.rs.ScriptC_ImageProcessing;
import com.realtouch.image.rs.ScriptC_Saturation;

import java.util.List;

@SuppressWarnings("deprecation")
@SuppressLint("NewApi")
public class RSCameraActivity extends Activity {

    private enum MenuType {
        Effect,
        Preview_Rotation,
        Preview_Size,
        Preview_Gravity
    }
    private MenuType menuType;

    /** 無 */
    private final static String Effect_None = "None(無)";
    /** 灰階 */
    private final static String Effect_Gray = "Mono(灰階)";
    /** 復古 */
    private final static String Effect_Sepia = "Sepia(復古)";
    /** 負片 */
    private final static String Effect_Negative = "Negative(負片)";
    /** 邊緣 */
    private final static String Effect_Edge = "Edge(邊緣)";
    /** 浮雕 */
    private final static String Effect_Emboss = "Emboss(浮雕)";
    /** 銳化 */
    private final static String Effect_Sharpen = "Sharpen(銳化)";
    /** 二值化 */
    private final static String Effect_Binary = "Binary(黑白)";
    /** 調整飽和度-Po Han */
    private final static String Effect_PH_Saturation = "Saturation_PH(飽和度)";
    /** 調整亮度-Po Han */
    private final static String Effect_PH_Bright = "Bright_PH(亮度)";
    /** 調整亮度 */
    private final static String Effect_Bright = "Bright(亮度)";
    /** 調整飽和度 */
    private final static String Effect_Saturation = "Saturation(飽和度)";

    private String effect = Effect_None;

    private final static String Orien_Auto = "自動";
    private final static String Orien_0 = "0度";
    private final static String Orien_90 = "90度";
    private final static String Orien_180 = "180度";
    private final static String Orien_270 = "270度";

    private final static String Gravity_Fit = "Fit";
    private final static String Gravity_Fill = "Fill";

    private MenuItem menu_Cancel;

    private DisplayMetrics metrics = new DisplayMetrics();

    private RSCameraView cv_camera;
    private View btn_switch;
    private View btn_effect;
    private View btn_preview_rotation;
    private View btn_preview_size;
    private View btn_preview_gravity;
    private SeekBar sb_values;

    @SuppressLint("InlinedApi")
    public static void FullScreencall(Window window) {

        if(Build.VERSION.SDK_INT < 19){ //19 or above api
            window.getDecorView().setSystemUiVisibility(View.GONE);
        } else {
            //for lower api versions.
            View decorView = window.getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//		Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this, CameraActivity2.class));

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        RSCameraActivity.FullScreencall(getWindow());
        setContentView(R.layout.activity_rscamera);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        initView();
    }

    protected void onResume() {
        super.onResume();

        cv_camera.startCamera(com.realtouch.camera.Constant.CAMERA_FACING_FRONT);

    }

    @Override
    protected void onPause() {
        super.onPause();

        cv_camera.stopCamera();
    }

    @Override
    protected void onDestroy() {

        unregisterForContextMenu(btn_effect);
        unregisterForContextMenu(btn_preview_rotation);
        unregisterForContextMenu(btn_preview_size);
        unregisterForContextMenu(btn_preview_gravity);

        super.onDestroy();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.btn_effect) {
            menuType = MenuType.Effect;

            String[] effects = {
                    Effect_None
                    ,Effect_Gray
                    ,Effect_Sepia
                    ,Effect_Negative
                    ,Effect_Edge
                    ,Effect_Emboss
                    ,Effect_Sharpen
                    ,Effect_Binary
                    ,Effect_PH_Saturation
                    ,Effect_PH_Bright
                    ,Effect_Bright
                    ,Effect_Saturation
            };
            for (int i = 0; i < effects.length; i ++) {
                menu.add(Menu.NONE, Menu.FIRST+i, Menu.NONE, effects[i]);
            }
            menu_Cancel = menu.add(Menu.NONE, Menu.FIRST+effects.length, Menu.NONE, "取消");
        }
        else if (v.getId() == R.id.btn_preview_rotation) {
            menuType = MenuType.Preview_Rotation;

            String[] oriens = {
                    Orien_Auto
                    ,Orien_0
                    ,Orien_90
                    ,Orien_180
                    ,Orien_270
            };
            for (int i = 0; i < oriens.length; i ++) {
                menu.add(Menu.NONE, Menu.FIRST+i, Menu.NONE, oriens[i]);
            }
            menu_Cancel = menu.add(Menu.NONE, Menu.FIRST+oriens.length, Menu.NONE, "取消");
        }
        else if (v.getId() == R.id.btn_preview_size) {
            menuType = MenuType.Preview_Size;

            int index = Menu.FIRST;
            List<Camera.Size> previewSizes = cv_camera.getSupportedPreviewSizes();
            if (previewSizes != null && previewSizes.size() > 0) {
                for (int i = 0; i < previewSizes.size(); i ++) {
                    Camera.Size previewSize = previewSizes.get(i);
                    menu.add(Menu.NONE, index, Menu.NONE, ""+previewSize.width+"x"+previewSize.height);

                    index ++;
                }
            }
            menu_Cancel = menu.add(Menu.NONE, index, Menu.NONE, "取消");
        }
        else if (v.getId() == R.id.btn_preview_gravity) {
            menuType = MenuType.Preview_Gravity;

            String[] gravities = {
                    Gravity_Fit
                    ,Gravity_Fill
            };
            for (int i = 0; i < gravities.length; i ++) {
                menu.add(Menu.NONE, Menu.FIRST+i, Menu.NONE, gravities[i]);
            }
            menu_Cancel = menu.add(Menu.NONE, Menu.FIRST+gravities.length, Menu.NONE, "取消");
        }

        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (!item.equals(menu_Cancel)) {
            if (menuType == MenuType.Effect) {
                effect = String.valueOf(item.getTitle());

                switch (effect) {
                    case Effect_Bright:
                        sb_values.setVisibility(View.VISIBLE);
                        sb_values.setMax(10);
                        sb_values.setProgress(5);
                        break;
                    case Effect_Saturation:
                        sb_values.setVisibility(View.VISIBLE);
                        sb_values.setMax(6);
                        sb_values.setProgress(2);
                        break;
                    default:
                        sb_values.setVisibility(View.GONE);
                        break;
                }
            }
            else if (menuType == MenuType.Preview_Rotation) {
                String title = String.valueOf(item.getTitle());
                switch (title) {
                    case Orien_Auto:
                        cv_camera.setPreviewOrientation(com.realtouch.camera.Constant.ORIENTATION_DEVICE);
                        break;
                    case Orien_0:
                        cv_camera.setPreviewOrientation(com.realtouch.camera.Constant.ORIENTATION_0);
                        break;
                    case Orien_90:
                        cv_camera.setPreviewOrientation(com.realtouch.camera.Constant.ORIENTATION_90);
                        break;
                    case Orien_180:
                        cv_camera.setPreviewOrientation(com.realtouch.camera.Constant.ORIENTATION_180);
                        break;
                    case Orien_270:
                        cv_camera.setPreviewOrientation(com.realtouch.camera.Constant.ORIENTATION_270);
                        break;
                }
            }
            else if (menuType == MenuType.Preview_Size) {
                int index = item.getItemId()-Menu.FIRST;
                List<Camera.Size> previewSizes = cv_camera.getSupportedPreviewSizes();
                if (previewSizes != null && index >= 0 && index < previewSizes.size()) {
                    Camera.Size previewSize = previewSizes.get(index);
                    cv_camera.setMaxPreviewSize(new com.realtouch.camera.Core.Size(previewSize));
                }
            }
            else if (menuType == MenuType.Preview_Gravity) {
                String title = String.valueOf(item.getTitle());
                if (title.equals(Gravity_Fit)) {
                    cv_camera.setPreviewGravity(com.realtouch.camera.Constant.PREVIEW_GRAVITY_RESIZE_ASPECT_FIT);
                }
                else if (title.equals(Gravity_Fill)) {
                    cv_camera.setPreviewGravity(com.realtouch.camera.Constant.PREVIEW_GRAVITY_RESIZE_ASPECT_FILL);
                }
            }
        }

        return super.onContextItemSelected(item);
    }

    private Toast currentToast;
    private void initView() {
        cv_camera = (RSCameraView) findViewById(R.id.cv_camera);
        cv_camera.setCameraStateListener(new com.realtouch.camera.ICamera.CameraStateListener() {

            @Override
            public void onPresetCamera(Camera camera, Camera.Parameters parameters) {
            }

            @Override
            public void onCameraStopped() {
            }

            @Override
            public void onCameraStarted(Camera camera) {
                RSCameraActivity.this.btn_switch.setEnabled(true);
                RSCameraActivity.this.btn_effect.setEnabled(true);
                RSCameraActivity.this.btn_preview_rotation.setEnabled(true);
                RSCameraActivity.this.btn_preview_size.setEnabled(true);
                RSCameraActivity.this.btn_preview_gravity.setEnabled(true);
            }

            @Override
            public void onCameraError(int errorCode, String errorMsg) {
				/*
				final String msg = errorMsg;
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						Toast.makeText(CameraActivity2.this, msg, Toast.LENGTH_LONG).show();
					}
				});
				*/
            }
        });
        cv_camera.setPreviewProcessor(new RSCameraView.PreviewProcessor() {

            @Override
            public void onProcessPreview(RenderScript rs, Allocation inputRGBA, Allocation outputRGBA) {

                synchronized (RSCameraActivity.this) {
                    try {
                        RSCameraActivity.this.doEffect(rs, inputRGBA, outputRGBA, RSCameraActivity.this.effect);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        });

        View btn_close = findViewById(R.id.btn_close);
        btn_close.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//				throw new NullPointerException();
                RSCameraActivity.this.finish();
            }
        });

        TextView tv_version = (TextView) findViewById(R.id.tv_version);
        PackageInfo pinfo;
        try {
            pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            tv_version.setText(pinfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        btn_switch = findViewById(R.id.btn_switch);
        if (com.realtouch.camera.Core.getNumberOfCameras() < 2) {
            btn_switch.setVisibility(View.GONE);
        }
        btn_switch.setEnabled(false);
        btn_switch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                RSCameraActivity.this.btn_switch.setEnabled(false);
                RSCameraActivity.this.btn_effect.setEnabled(false);
                RSCameraActivity.this.btn_preview_rotation.setEnabled(false);
                RSCameraActivity.this.btn_preview_size.setEnabled(false);
                RSCameraActivity.this.btn_preview_gravity.setEnabled(false);

                int facing = RSCameraActivity.this.cv_camera.getCameraFacing();
                if (facing == com.realtouch.camera.Constant.CAMERA_FACING_BACK) {
                    RSCameraActivity.this.cv_camera.setCameraFacing(com.realtouch.camera.Constant.CAMERA_FACING_FRONT);
                }
                else {
                    RSCameraActivity.this.cv_camera.setCameraFacing(com.realtouch.camera.Constant.CAMERA_FACING_BACK);
                }
            }
        });

        btn_effect = findViewById(R.id.btn_effect);
        btn_effect.setEnabled(false);
        registerForContextMenu(btn_effect);
        btn_effect.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                RSCameraActivity.this.openContextMenu(v);
            }
        });

        btn_preview_rotation = findViewById(R.id.btn_preview_rotation);
        btn_preview_rotation.setEnabled(false);
        registerForContextMenu(btn_preview_rotation);
        btn_preview_rotation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                RSCameraActivity.this.openContextMenu(v);
            }
        });

        btn_preview_size = findViewById(R.id.btn_preview_size);
        btn_preview_size.setEnabled(false);
        registerForContextMenu(btn_preview_size);
        btn_preview_size.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                RSCameraActivity.this.openContextMenu(v);
            }
        });

        btn_preview_gravity = findViewById(R.id.btn_preview_gravity);
        btn_preview_gravity.setEnabled(false);
        registerForContextMenu(btn_preview_gravity);
        btn_preview_gravity.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                RSCameraActivity.this.openContextMenu(v);
            }
        });

        sb_values = (SeekBar) findViewById(R.id.sb_values);
        sb_values.setVisibility(View.GONE);
        sb_values.setMax(10);
        sb_values.setProgress(5);
        sb_values.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    if (RSCameraActivity.this.effect.equals(RSCameraActivity.Effect_Bright)) {
                        if (RSCameraActivity.this.currentToast != null) {
                            try {
                                RSCameraActivity.this.currentToast.cancel();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        RSCameraActivity.this.currentToast = null;

                        float weight = 1f+(progress-seekBar.getMax()/2f)/5f;
                        weight = Math.max(weight, 0.1f);
                        RSCameraActivity.this.currentToast = Toast.makeText(RSCameraActivity.this, "weight="+weight, Toast.LENGTH_SHORT);
                        RSCameraActivity.this.currentToast.show();
                    }
                    else if (RSCameraActivity.this.effect.equals(RSCameraActivity.Effect_Saturation)) {
                        if (RSCameraActivity.this.currentToast != null) {
                            try {
                                RSCameraActivity.this.currentToast.cancel();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        RSCameraActivity.this.currentToast = null;

                        float change = progress/2f;
                        RSCameraActivity.this.currentToast = Toast.makeText(RSCameraActivity.this, "change="+change, Toast.LENGTH_SHORT);
                        RSCameraActivity.this.currentToast.show();
                    }
                }
            }
        });

    }

    private void doEffect(RenderScript rs, Allocation in, Allocation out, String effect) {
        switch (effect) {
            case Effect_Gray: {
                ScriptIntrinsicColorMatrix script = ScriptIntrinsicColorMatrix.create(rs);
                Matrix4f colorMat = new Matrix4f(new float[]{0.56f, 0.56f, 0.56f, 0,
                                                            0.33f, 0.33f, 0.33f, 0,
                                                            0.11f, 0.11f, 0.11f, 0,
                                                            0, 0, 0, 1});
                script.setColorMatrix(colorMat);
                script.forEach(in, out);
                script.destroy();
                break;
            }
            case Effect_Sepia: {
                ScriptIntrinsicColorMatrix script = ScriptIntrinsicColorMatrix.create(rs);
//			    Matrix4f colorMat = new Matrix4f(new float[]{0.393f, 0.349f, 0.56f, 0,
//														    0.769f, 0.686f, 0.168f, 0,
//														    0.272f, 0.534f, 0.131f, 0,
//														    0, 0, 0, 1});
                Matrix4f colorMat = new Matrix4f(new float[]{0.393f, 0.349f, 0.272f, 0,
                                                            0.769f, 0.686f, 0.534f, 0,
                                                            0.189f, 0.168f, 0.131f, 0,
                                                            0, 0, 0, 1});
                script.setColorMatrix(colorMat);
                script.forEach(in, out);
                script.destroy();
                break;
            }
            case Effect_Negative: {
                ScriptIntrinsicColorMatrix script = ScriptIntrinsicColorMatrix.create(rs);
                Matrix4f colorMat = new Matrix4f(new float[]{-1f, 0, 0, 0,
                                                            0, -1f, 0, 0,
                                                            0, 0, -1f, 0,
                                                            0, 0, 0, 1});
                script.setColorMatrix(colorMat);
                script.setAdd(1f, 1f, 1f, 0);
                script.forEach(in, out);
                script.destroy();
                break;
            }
            case Effect_Edge: {
                ScriptIntrinsicConvolve3x3 script = ScriptIntrinsicConvolve3x3.create(rs, in.getElement());
                float[] coefficients = {-1, -1, -1,
                                        -1, 8, -1,
                                        -1, -1, -1}; // edge detection
                script.setCoefficients(coefficients);
                script.setInput(in);
                script.forEach(out);
                script.destroy();
                break;
            }
            case Effect_Emboss: {
                ScriptIntrinsicConvolve3x3 script = ScriptIntrinsicConvolve3x3.create(rs, in.getElement());
                float[] coefficients = {-1, -1, 0,
                                        -1, 0, 1,
                                        0, 1, 1}; // embossing
                script.setCoefficients(coefficients);
                script.setInput(in);
                script.forEach(out);
                in.copyFrom(out);

                // convert to gray scale
                ScriptIntrinsicColorMatrix script2 = ScriptIntrinsicColorMatrix.create(rs);
                Matrix4f colorMat = new Matrix4f(new float[]{0.56f, 0.56f, 0.56f, 0,
                                                            0.33f, 0.33f, 0.33f, 0,
                                                            0.11f, 0.11f, 0.11f, 0,
                                                            0, 0, 0, 1});
                script2.setColorMatrix(colorMat);
                script2.setAdd(0.5f, 0.5f, 0.5f, 0);
                script2.forEach(in, out);

                script2.destroy();
                script.destroy();
                break;
            }
            case Effect_Sharpen: {
                ScriptIntrinsicConvolve3x3 script = ScriptIntrinsicConvolve3x3.create(rs, in.getElement());
                float[] coefficients = {1, 1, 1,
                                        1, -7, 1,
                                        1, 1, 1}; // sharpen
                script.setCoefficients(coefficients);
                script.setInput(in);
                script.forEach(out);
                script.destroy();
                break;
            }
            case Effect_Binary: {
                ScriptC_ImageProcessing script = new ScriptC_ImageProcessing(rs, getResources(), R.raw.imageprocessing);
                script.set_binaryThreshold(0.2f);
                script.forEach_binary_rgba8888(in, out);
                script.destroy();
                break;
            }
            case Effect_PH_Saturation: {
                ScriptC_Saturation script = new ScriptC_Saturation(rs, getResources(), R.raw.saturation);
                script.set_saturationWeight(0.05f);
                script.invoke_adjustSaturation(script, in, out);
                script.destroy();

                // faster
//                ScriptC_ImageProcessing script = new ScriptC_ImageProcessing(rs, getResources(), R.raw.imageprocessing);
//                script.set_saturationWeight(0.05f);
//                script.forEach_saturation_rgba8888(in, out);
//                script.destroy();
                break;
            }
            case Effect_PH_Bright: {
                ScriptC_Bright script = new ScriptC_Bright(rs, getResources(), R.raw.bright);
                script.set_brightStrength(128);
                script.invoke_setInput(in);
                script.invoke_adjustBright(script, out);
                script.destroy();
                break;
            }
            case Effect_Bright: {
                float weight = 1f + (sb_values.getProgress() - sb_values.getMax() / 2f) / 5f;
                weight = Math.max(weight, 0.1f);

                ScriptIntrinsicColorMatrix script = ScriptIntrinsicColorMatrix.create(rs);
                Matrix4f colorMat = new Matrix4f(new float[]{weight, 0, 0, 0,
                                                            0, weight, 0, 0,
                                                            0, 0, weight, 0,
                                                            0, 0, 0, 1});
                script.setColorMatrix(colorMat);
                script.forEach(in, out);
                script.destroy();
                break;
            }
            case Effect_Saturation: {
                ScriptC_Saturation script = new ScriptC_Saturation(rs, getResources(), R.raw.saturation);
                script.set_change(sb_values.getProgress() / 2f);

                script.forEach_simple_saturation(in, out);
                break;
            }
            default:
                out.copyFrom(in);
                break;
        }

    }

}
