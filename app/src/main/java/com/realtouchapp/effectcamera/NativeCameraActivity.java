package com.realtouchapp.effectcamera;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.realtouch.camera.NativeCameraView;

import java.util.List;

@SuppressWarnings("deprecation")
@SuppressLint("NewApi")
public class NativeCameraActivity extends Activity {

    private enum MenuType {
        Effect,
        Preview_Rotation,
        Preview_Size,
        Preview_Gravity
    }
    private MenuType menuType;

    private final static String Orien_Auto = "自動";
    private final static String Orien_0 = "0度";
    private final static String Orien_90 = "90度";
    private final static String Orien_180 = "180度";
    private final static String Orien_270 = "270度";

    private final static String Gravity_Fit = "Fit";
    private final static String Gravity_Fill = "Fill";

    private MenuItem menu_Cancel;

    private DisplayMetrics metrics = new DisplayMetrics();

    private NativeCameraView cv_camera;
    private View btn_switch;
    private View btn_effect;
    private View btn_preview_rotation;
    private View btn_preview_size;
    private View btn_preview_gravity;
    private SeekBar sb_values;

    @SuppressLint("InlinedApi")
    public static void FullScreencall(Window window) {

        if(Build.VERSION.SDK_INT < 19){ //19 or above api
            window.getDecorView().setSystemUiVisibility(View.GONE);
        } else {
            //for lower api versions.
            View decorView = window.getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//		Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this, CameraActivity2.class));

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        NativeCameraActivity.FullScreencall(getWindow());
        setContentView(R.layout.activity_native_camera);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        initView();
    }

    protected void onResume() {
        super.onResume();

        cv_camera.startCamera(com.realtouch.camera.Constant.CAMERA_FACING_FRONT);

    }

    @Override
    protected void onPause() {
        super.onPause();

        cv_camera.stopCamera();
    }

    @Override
    protected void onDestroy() {

        unregisterForContextMenu(btn_effect);
        unregisterForContextMenu(btn_preview_rotation);
        unregisterForContextMenu(btn_preview_size);
        unregisterForContextMenu(btn_preview_gravity);

        super.onDestroy();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.btn_effect) {
            menuType = MenuType.Effect;

            menu_Cancel = menu.add(Menu.NONE, Menu.FIRST, Menu.NONE, "取消");
        }
        else if (v.getId() == R.id.btn_preview_rotation) {
            menuType = MenuType.Preview_Rotation;

            String[] oriens = {
                    Orien_Auto
                    ,Orien_0
                    ,Orien_90
                    ,Orien_180
                    ,Orien_270
            };
            for (int i = 0; i < oriens.length; i ++) {
                menu.add(Menu.NONE, Menu.FIRST+i, Menu.NONE, oriens[i]);
            }
            menu_Cancel = menu.add(Menu.NONE, Menu.FIRST+oriens.length, Menu.NONE, "取消");
        }
        else if (v.getId() == R.id.btn_preview_size) {
            menuType = MenuType.Preview_Size;

            int index = Menu.FIRST;
            List<Camera.Size> previewSizes = cv_camera.getSupportedPreviewSizes();
            if (previewSizes != null && previewSizes.size() > 0) {
                for (int i = 0; i < previewSizes.size(); i ++) {
                    Camera.Size previewSize = previewSizes.get(i);
                    menu.add(Menu.NONE, index, Menu.NONE, ""+previewSize.width+"x"+previewSize.height);

                    index ++;
                }
            }
            menu_Cancel = menu.add(Menu.NONE, index, Menu.NONE, "取消");
        }
        else if (v.getId() == R.id.btn_preview_gravity) {
            menuType = MenuType.Preview_Gravity;

            String[] gravities = {
                    Gravity_Fit
                    ,Gravity_Fill
            };
            for (int i = 0; i < gravities.length; i ++) {
                menu.add(Menu.NONE, Menu.FIRST+i, Menu.NONE, gravities[i]);
            }
            menu_Cancel = menu.add(Menu.NONE, Menu.FIRST+gravities.length, Menu.NONE, "取消");
        }

        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (!item.equals(menu_Cancel)) {
            if (menuType == MenuType.Effect) {

            }
            else if (menuType == MenuType.Preview_Rotation) {
                String title = String.valueOf(item.getTitle());
                switch (title) {
                    case Orien_Auto:
                        cv_camera.setPreviewOrientation(com.realtouch.camera.Constant.ORIENTATION_DEVICE);
                        break;
                    case Orien_0:
                        cv_camera.setPreviewOrientation(com.realtouch.camera.Constant.ORIENTATION_0);
                        break;
                    case Orien_90:
                        cv_camera.setPreviewOrientation(com.realtouch.camera.Constant.ORIENTATION_90);
                        break;
                    case Orien_180:
                        cv_camera.setPreviewOrientation(com.realtouch.camera.Constant.ORIENTATION_180);
                        break;
                    case Orien_270:
                        cv_camera.setPreviewOrientation(com.realtouch.camera.Constant.ORIENTATION_270);
                        break;
                }
            }
            else if (menuType == MenuType.Preview_Size) {
                int index = item.getItemId()-Menu.FIRST;
                List<Camera.Size> previewSizes = cv_camera.getSupportedPreviewSizes();
                if (previewSizes != null && index >= 0 && index < previewSizes.size()) {
                    Camera.Size previewSize = previewSizes.get(index);
                    cv_camera.setMaxPreviewSize(new com.realtouch.camera.Core.Size(previewSize));
                }
            }
            else if (menuType == MenuType.Preview_Gravity) {
                String title = String.valueOf(item.getTitle());
                if (title.equals(Gravity_Fit)) {
                    cv_camera.setPreviewGravity(com.realtouch.camera.Constant.PREVIEW_GRAVITY_RESIZE_ASPECT_FIT);
                }
                else if (title.equals(Gravity_Fill)) {
                    cv_camera.setPreviewGravity(com.realtouch.camera.Constant.PREVIEW_GRAVITY_RESIZE_ASPECT_FILL);
                }
            }
        }

        return super.onContextItemSelected(item);
    }

    private Toast currentToast;
    private void initView() {
        cv_camera = (NativeCameraView) findViewById(R.id.cv_camera);
        cv_camera.setCameraStateListener(new com.realtouch.camera.ICamera.CameraStateListener() {

            @Override
            public void onPresetCamera(Camera camera, Camera.Parameters parameters) {
            }

            @Override
            public void onCameraStopped() {
            }

            @Override
            public void onCameraStarted(Camera camera) {
                NativeCameraActivity.this.btn_switch.setEnabled(true);
                NativeCameraActivity.this.btn_effect.setEnabled(true);
                NativeCameraActivity.this.btn_preview_rotation.setEnabled(true);
                NativeCameraActivity.this.btn_preview_size.setEnabled(true);
                NativeCameraActivity.this.btn_preview_gravity.setEnabled(true);
            }

            @Override
            public void onCameraError(int errorCode, String errorMsg) {
				/*
				final String msg = errorMsg;
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						Toast.makeText(CameraActivity2.this, msg, Toast.LENGTH_LONG).show();
					}
				});
				*/
            }
        });

        View btn_close = findViewById(R.id.btn_close);
        btn_close.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//				throw new NullPointerException();
                NativeCameraActivity.this.finish();
            }
        });

        TextView tv_version = (TextView) findViewById(R.id.tv_version);
        PackageInfo pinfo;
        try {
            pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            tv_version.setText(pinfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        btn_switch = findViewById(R.id.btn_switch);
        if (com.realtouch.camera.Core.getNumberOfCameras() < 2) {
            btn_switch.setVisibility(View.GONE);
        }
        btn_switch.setEnabled(false);
        btn_switch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                NativeCameraActivity.this.btn_switch.setEnabled(false);
                NativeCameraActivity.this.btn_effect.setEnabled(false);
                NativeCameraActivity.this.btn_preview_rotation.setEnabled(false);
                NativeCameraActivity.this.btn_preview_size.setEnabled(false);
                NativeCameraActivity.this.btn_preview_gravity.setEnabled(false);

                int facing = NativeCameraActivity.this.cv_camera.getCameraFacing();
                if (facing == com.realtouch.camera.Constant.CAMERA_FACING_BACK) {
                    NativeCameraActivity.this.cv_camera.setCameraFacing(com.realtouch.camera.Constant.CAMERA_FACING_FRONT);
                }
                else {
                    NativeCameraActivity.this.cv_camera.setCameraFacing(com.realtouch.camera.Constant.CAMERA_FACING_BACK);
                }
            }
        });

        btn_effect = findViewById(R.id.btn_effect);
        btn_effect.setEnabled(false);
        registerForContextMenu(btn_effect);
        btn_effect.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                NativeCameraActivity.this.openContextMenu(v);
            }
        });

        btn_preview_rotation = findViewById(R.id.btn_preview_rotation);
        btn_preview_rotation.setEnabled(false);
        registerForContextMenu(btn_preview_rotation);
        btn_preview_rotation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                NativeCameraActivity.this.openContextMenu(v);
            }
        });

        btn_preview_size = findViewById(R.id.btn_preview_size);
        btn_preview_size.setEnabled(false);
        registerForContextMenu(btn_preview_size);
        btn_preview_size.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                NativeCameraActivity.this.openContextMenu(v);
            }
        });

        btn_preview_gravity = findViewById(R.id.btn_preview_gravity);
        btn_preview_gravity.setEnabled(false);
        registerForContextMenu(btn_preview_gravity);
        btn_preview_gravity.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                NativeCameraActivity.this.openContextMenu(v);
            }
        });

        sb_values = (SeekBar) findViewById(R.id.sb_values);
        sb_values.setVisibility(View.GONE);

    }

}
