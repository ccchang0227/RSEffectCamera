package com.realtouch.camera;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.Camera;
import android.view.Surface;
import android.view.WindowManager;

@SuppressWarnings({"deprecation", "unused"})
public final class Core {
	@SuppressWarnings("deprecation")
	public static class Size {
		public int width = 0;
		public int height = 0;
		
		public Size(int width, int height) {
			super();
			this.width = width;
			this.height = height;
		}
		
		public Size(Camera.Size size) {
			super();
			this.width = size.width;
			this.height = size.height;
		}
		
		public Size(SizeF size) {
			super();
			this.width = (int) size.width;
			this.height = (int) size.height;
		}
		
		@Override
		public String toString() {
			return "Size("+this.width+", "+this.height+")";
		}
		
	}
	@SuppressWarnings("deprecation")
	public static class SizeF {
		public float width = 0;
		public float height = 0;
		
		public SizeF(float width, float height) {
			super();
			this.width = width;
			this.height = height;
		}
		
		public SizeF(Camera.Size size) {
			super();
			this.width = (float) size.width;
			this.height = (float) size.height;
		}
		
		public SizeF(Size size) {
			super();
			this.width = (float) size.width;
			this.height = (float) size.height;
		}
		
		@Override
		public String toString() {
			return "SizeF("+this.width+", "+this.height+")";
		}
	}
	
	/**
	 * 取得裝置的相機數
	 * 
	 * @return 相機總數
	 */
	public static int getNumberOfCameras() {
		return Camera.getNumberOfCameras();
	}
	
	/**
	 * 計算最適合當前裝置方向的相機預覽角度
	 * 
	 * @param context 上下文
	 * @param cameraID 相機id
	 * @return 預覽角度，0度~359度
	 */
	public static int getPreferredPreviewOrientation(Context context, int cameraID) {
		if (cameraID < 0 || cameraID >= getNumberOfCameras()) {
			return 0;
		}
		
		Camera.CameraInfo info = new Camera.CameraInfo();
	    Camera.getCameraInfo(cameraID, info);
		
	    int rotation = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();
		int degrees = 0;
		switch (rotation) {
		case Surface.ROTATION_0: 
			degrees = 0; 
			break;
		case Surface.ROTATION_90: 
			degrees = 90; 
			break;
		case Surface.ROTATION_180: 
			degrees = 180; 
			break;
		case Surface.ROTATION_270: 
			degrees = 270; 
			break;
		}
		
		int result;
		if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
			// front-facing
			result = (info.orientation + degrees) % 360;
			result = (360 - result) % 360;  // compensate the mirror
		}
		else {
			// back-facing
			result = (info.orientation - degrees + 360) % 360;
		}
		
		return (result%360);
	}
	
	/**
	 * 計算等比例寬高fit在給定矩形框內的位置和大小<br>
	 * 位置在rect內為置中
	 * 
	 * @param aspectSize 等比例顯示的寬高
	 * @param boundingRect 限制大小的矩形
	 * @return aspectSize在boundingRect內居中fit顯示的位置和大小
	 */
	public static Rect makeAspectFitRect(Size aspectSize, Rect boundingRect) {
		if (aspectSize == null && boundingRect == null) {
			return null;
		}
		else if (aspectSize == null) {
			return boundingRect;
		}
		else if (boundingRect == null) {
			return new Rect(0, 0, aspectSize.width, aspectSize.height);
		}
		
		int width = aspectSize.width;
		int height = aspectSize.height;
	    float scale = (float)boundingRect.width()/(float)width;
	    if (scale*height >= boundingRect.height()) {
	        scale = (float)boundingRect.height()/(float)height;
	    }
	    int targetWidth = (int) (aspectSize.width*scale);
	    int targetHeight = (int) (aspectSize.height*scale);
	    
	    Rect targetRect = new Rect();
	    targetRect.left = boundingRect.left + (boundingRect.width()-targetWidth)/2;
	    targetRect.top = boundingRect.top + (boundingRect.height()-targetHeight)/2;
	    targetRect.right = targetRect.left+targetWidth;
	    targetRect.bottom = targetRect.top+targetHeight;
	    return targetRect;
	}
	
	/**
	 * 計算等比例寬高fit在給定矩形框內的位置和大小<br>
	 * 位置在rect內為置中
	 * 
	 * @param aspectSize 等比例顯示的寬高
	 * @param boundingRect 限制大小的矩形
	 * @return aspectSize在boundingRect內居中fit顯示的位置和大小
	 */
	public static RectF makeAspectFitRect(SizeF aspectSize, RectF boundingRect) {
		if (aspectSize == null && boundingRect == null) {
			return null;
		}
		else if (aspectSize == null) {
			return boundingRect;
		}
		else if (boundingRect == null) {
			return new RectF(0, 0, aspectSize.width, aspectSize.height);
		}
		
		float width = aspectSize.width;
	    float height = aspectSize.height;
	    float scale = boundingRect.width()/width;
	    if (scale*height >= boundingRect.height()) {
	        scale = boundingRect.height()/height;
	    }
	    float targetWidth = aspectSize.width*scale;
	    float targetHeight = aspectSize.height*scale;
	    
	    RectF targetRect = new RectF();
	    targetRect.left = boundingRect.left + (boundingRect.width()-targetWidth)/2f;
	    targetRect.top = boundingRect.top + (boundingRect.height()-targetHeight)/2f;
	    targetRect.right = targetRect.left+targetWidth;
	    targetRect.bottom = targetRect.top+targetHeight;
	    return targetRect;
	}
	
	/**
	 * 計算等比例寬高fill在給定矩形框內的位置和大小 (上下或左右會被切掉)<br>
	 * 位置在rect內為置中
	 * 
	 * @param aspectSize 等比例顯示的寬高
	 * @param boundingRect 限制大小的矩形
	 * @return aspectSize在boundingRect內居中fill顯示的位置和大小
	 */
	public static Rect makeAspectFillRect(Size aspectSize, Rect boundingRect) {
		if (aspectSize == null && boundingRect == null) {
			return null;
		}
		else if (aspectSize == null) {
			return boundingRect;
		}
		else if (boundingRect == null) {
			return new Rect(0, 0, aspectSize.width, aspectSize.height);
		}
		
		int width = aspectSize.width;
		int height = aspectSize.height;
	    float scale = (float)boundingRect.width()/(float)width;
	    if (scale*height < boundingRect.height()) {
	        scale = (float)boundingRect.height()/(float)height;
	    }
	    int targetWidth = (int) (aspectSize.width*scale);
	    int targetHeight = (int) (aspectSize.height*scale);
	    
	    Rect targetRect = new Rect();
	    targetRect.left = boundingRect.left + (boundingRect.width()-targetWidth)/2;
	    targetRect.top = boundingRect.top + (boundingRect.height()-targetHeight)/2;
	    targetRect.right = targetRect.left+targetWidth;
	    targetRect.bottom = targetRect.top+targetHeight;
	    return targetRect;
	}
	
	/**
	 * 計算等比例寬高fill在給定矩形框內的位置和大小 (上下或左右會被切掉)<br>
	 * 位置在rect內為置中
	 * 
	 * @param aspectSize 等比例顯示的寬高
	 * @param boundingRect 限制大小的矩形
	 * @return aspectSize在boundingRect內居中fill顯示的位置和大小
	 */
	public static RectF makeAspectFillRect(SizeF aspectSize, RectF boundingRect) {
		if (aspectSize == null && boundingRect == null) {
			return null;
		}
		else if (aspectSize == null) {
			return boundingRect;
		}
		else if (boundingRect == null) {
			return new RectF(0, 0, aspectSize.width, aspectSize.height);
		}
		
		float width = aspectSize.width;
	    float height = aspectSize.height;
	    float scale = boundingRect.width()/width;
	    if (scale*height < boundingRect.height()) {
	        scale = boundingRect.height()/height;
	    }
	    float targetWidth = aspectSize.width*scale;
	    float targetHeight = aspectSize.height*scale;
	    
	    RectF targetRect = new RectF();
	    targetRect.left = boundingRect.left + (boundingRect.width()-targetWidth)/2f;
	    targetRect.top = boundingRect.top + (boundingRect.height()-targetHeight)/2f;
	    targetRect.right = targetRect.left+targetWidth;
	    targetRect.bottom = targetRect.top+targetHeight;
	    return targetRect;
	}
	
}
