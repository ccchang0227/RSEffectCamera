package com.realtouch.camera;

import android.hardware.Camera;

import java.util.List;

@SuppressWarnings({"deprecation", "UnnecessaryInterfaceModifier", "unused"})
public interface ICamera {
	@SuppressWarnings({"deprecation", "UnnecessaryInterfaceModifier"})
	public static interface CameraStateListener {
		public void onCameraStarted(Camera camera);
		public void onPresetCamera(Camera camera, Camera.Parameters parameters);
		public void onCameraStopped();
		public void onCameraError(int errorCode, String errorMsg);
	}
	
	public Camera getCamera();
	
	public CameraStateListener getCameraStateListener();
	public void setCameraStateListener(CameraStateListener listener);
	
	public void startCamera();
	public void startCamera(int cameraFacing);
	public void stopCamera();
	public boolean isCameraRunning();

	public void startPreview();
	public void stopPreview();

	public void setDefaultParameters(Camera.Parameters parameters);
	
	public void setCameraFacing(int cameraFacing);
	public int getCameraFacing();
	
	public void setMaxPreviewSize(Core.Size maxPreviewSize);
	public Camera.Size getPreviewSize();
	public void setMaxPictureSize(Core.Size maxPictureSize);
	public Camera.Size getPictureSize();
	
	public void setPreviewOrientation(int orientation);
	public int getPreviewOrientation();
	
	public List<Camera.Size> getSupportedPreviewSizes();
	public List<Camera.Size> getSupportedPictureSizes();
	public List<Camera.Size> getSupportedVideoSizes();
	public List<String> getSupportedFlashModes();
	public List<String> getSupportedFocusModes();
	public List<String> getSupportedWhiteBalance();
	public List<String> getSupportedAntibanding();
	public List<String> getSupportedColorEffects();
	public List<String> getSupportedSceneModes();
	
}
