package com.realtouch.camera.renderscript;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.Paint;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicYuvToRGB;
import android.renderscript.Type;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import com.realtouch.camera.CameraViewBase;
import com.realtouch.camera.Constant;
import com.realtouch.camera.Core;
import com.realtouch.camera.Core.Size;
import com.realtouch.camera.Session.PreviewFrameCallback;

import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Locale;

@SuppressWarnings({"deprecation", "unused"})
public class RSCameraView extends CameraViewBase {
	@SuppressWarnings("UnnecessaryInterfaceModifier")
	public interface PreviewProcessor {
		public void onProcessPreview(RenderScript rs, Allocation inputRGBA, Allocation outputRGBA);
	}
	
	@SuppressWarnings("unused")
	private boolean mSurfaceCreated = false;
	private SurfaceView mSurfaceView = null;
	private SurfaceHolder.Callback mSurfaceCallback = new SurfaceHolder.Callback() {
		
		@Override
		public void surfaceDestroyed(SurfaceHolder holder) {
			RSCameraView.this.stopCamera();

			RSCameraView.this.mSurfaceCreated = false;
		}
		
		@Override
		public void surfaceCreated(SurfaceHolder holder) {
			RSCameraView.this.mSurfaceCreated = true;
		}
		
		@Override
		public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		}
	};

	private long mPreviewStartTime = 0;
	private Thread mDrawingThread = null;
	private PreviewFrame mPreviewFrame = null;
	private PreviewProcessor mPreviewProcessor = null;
	private PreviewFrameCallback mFrameCallback = new PreviewFrameCallback() {
		
		@Override
		public void onProcessPreview(byte[] yuv420sp, Camera.Size previewSize, int previewFormat) {
			if (yuv420sp == null || yuv420sp.length == 0) {
				return;
			}
			
			RSCameraView.this.checkPreviewSizeChanged(previewSize);
			Bitmap bitmap = RSCameraView.this.mPreviewFrame.getBitmap();
			
			int size = (previewSize.width * previewSize.height * ImageFormat.getBitsPerPixel(previewFormat)) / 8;
	        if (bitmap.getWidth() != previewSize.width || bitmap.getHeight() != previewSize.height || size > yuv420sp.length) {
	        	Canvas canvas = new Canvas(bitmap);
				canvas.drawColor(Color.BLACK);
	        }
	        else {
	        	RenderScript rs = RenderScript.create(RSCameraView.this.mContext);
				Type yuvType = new Type.Builder(rs, Element.U8(rs)).setX(size).create();
//				Type yuvType = new Type.Builder(rs, Element.YUV(rs)).setX(size).create();
			    Allocation yuvBuffer = Allocation.createTyped(rs, yuvType, Allocation.USAGE_SCRIPT|Allocation.USAGE_SHARED);
			    byte[] subData = new byte[size];
			    System.arraycopy(yuv420sp, 0, subData, 0, size);
			    yuvBuffer.copy1DRangeFrom(0, size, subData);
			    
			    Type rgbaType = new Type.Builder(rs, Element.RGBA_8888(rs))
//			    								.setX(previewSize.width)
//			    								.setY(previewSize.height)
									    		.setX(bitmap.getWidth())
												.setY(bitmap.getHeight())
			    								.create();
			    Allocation rgbaBuffer = Allocation.createTyped(rs, rgbaType, Allocation.USAGE_SCRIPT|Allocation.USAGE_SHARED);
				
			    ScriptIntrinsicYuvToRGB yuv2Rgb = ScriptIntrinsicYuvToRGB.create(rs, Element.U8_4(rs));
			    yuv2Rgb.setInput(yuvBuffer);
			    yuv2Rgb.forEach(rgbaBuffer);
			    
			    yuvBuffer.destroy();
			    yuvType.destroy();
			    yuv2Rgb.destroy();
			    
				Allocation inAlloc = Allocation.createTyped(rs, rgbaType, Allocation.USAGE_SCRIPT|Allocation.USAGE_SHARED);
				inAlloc.copyFrom(rgbaBuffer);
				if (RSCameraView.this.mPreviewProcessor != null) {
					try {
						RSCameraView.this.mPreviewProcessor.onProcessPreview(rs, inAlloc, rgbaBuffer);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				inAlloc.destroy();
				
				rgbaBuffer.copyTo(bitmap);
			    rgbaBuffer.destroy();
			    rgbaType.destroy();
			    rs.destroy();
	        }
	        
	        synchronized (RSCameraView.this) {
	        	RSCameraView.this.notify();
            }
		}
	};

	public RSCameraView(Context context) {
		this(context, null);
	}
	
	public RSCameraView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}
	
	public RSCameraView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		mSurfaceView = new SurfaceView(context);
		addView(mSurfaceView);
		SurfaceHolder surfaceHolder = mSurfaceView.getHolder();
		surfaceHolder.addCallback(mSurfaceCallback);

	}
	
	@Override
	protected void finalize() throws Throwable {
		
		try {
			if (mDrawingThread != null && mDrawingThread.isAlive() && !mDrawingThread.isInterrupted()) {
				mDrawingThread.interrupt();
			}
			mDrawingThread = null;
			mPreviewFrame = null;
			mPreviewProcessor = null;
			mFrameCallback = null;

			mSurfaceView = null;
			mSurfaceCallback = null;

		} catch (Throwable e) {
			e.printStackTrace();
		}
		
		super.finalize();
	}
	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		if (changed) {
			onLayoutChanged(this, l, t, r, b, 0, 0, 0, 0);
		}
	}

	public PreviewProcessor getPreviewProcessor() {
		return mPreviewProcessor;
	}
	
	public void setPreviewProcessor(PreviewProcessor previewProcessor) {
		this.mPreviewProcessor = previewProcessor;
	}

	// -- CameraViewBase

	@Override
	protected void onLayoutChanged(View view, int nl, int nt, int nr, int nb, int ol, int ot, int or, int ob) {
		if (view == this && getChildCount() > 0) {
			int width = nr - nl;
			int height = nb - nt;
			mSurfaceView.layout(0, 0, width, height);
		}
	}

	@Override
	protected void onPresetCamera(Camera camera, Parameters parameters) {
		// 設定預設參數
		mSession.setDefaultParameters(parameters);

		// 先停止舊的繪圖Thread (如果存在的話)
		if (mDrawingThread != null && mDrawingThread.isAlive() && !mDrawingThread.isInterrupted()) {
			mDrawingThread.interrupt();
		}
		mDrawingThread = null;

		// 啟動previewCallback
		boolean callbackEnabled = mSession.enablePreviewCallback(mFrameCallback);
		if (callbackEnabled) {
			// 啟動繪圖Thread
			mPreviewStartTime = 0;
			mDrawingThread = new Thread(new CameraWorker());
			mDrawingThread.start();
		}
	}

	@Override
	protected void onCameraStarted(Camera camera) {}

	@Override
	protected void onCameraStopped() {
		// 停止previewCallback
		mSession.disablePreviewCallback();
		// 停止繪圖Thread
		if (mDrawingThread != null && mDrawingThread.isAlive() && !mDrawingThread.isInterrupted()) {
			mDrawingThread.interrupt();
		}
		mDrawingThread = null;
		mPreviewFrame = null;

		// 把畫面塗黑
		SurfaceHolder holder = mSurfaceView.getHolder();
		if (holder.getSurface() != null) {
			Canvas canvas = null;
			try {
				canvas = holder.lockCanvas();
				canvas.drawColor(Color.BLACK);
				holder.unlockCanvasAndPost(canvas);
			} catch (Exception e) {
				e.printStackTrace();

				if (canvas != null) {
					holder.unlockCanvasAndPost(canvas);
				}
			}
		}

	}

	@Override
	protected void onCameraError(int errorCode, String errorMsg) {}

	// -- Private --
	
	private void checkPreviewSizeChanged(Camera.Size previewSize) {
		if (mPreviewFrame == null) {
        	mPreviewFrame = new PreviewFrame();
        }
        
        if (previewSize.width != mPreviewFrame.getWidth() || previewSize.height != mPreviewFrame.getHeight()) {
        	mPreviewFrame.setSize(previewSize.width, previewSize.height);
        }
	}
	
	private void onDeliverAndDrawFrame(PreviewFrame frame) {
		float fps = 0;
		if (mPreviewStartTime == 0) {
			mPreviewStartTime = System.currentTimeMillis();
		}
		else {
			long diff = System.currentTimeMillis()-mPreviewStartTime;
			fps = 1000f/(float)diff;
			mPreviewStartTime = System.currentTimeMillis();
		}
		
		SurfaceHolder holder = mSurfaceView.getHolder();
		if (holder.getSurface() != null) {
			Canvas canvas = null;
			try {
				canvas = holder.lockCanvas();
				canvas.drawColor(Color.BLACK);
				canvas.save();
				
				int drawWidth = canvas.getWidth();
				int drawHeight = canvas.getHeight();
				
				int facing = mCameraFacing;
				int orientation = mCameraPreviewOrientation%360;
				switch (orientation) {
				case 90: {
					canvas.rotate(90);
					if (facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
						canvas.translate(0, -canvas.getWidth());
						canvas.scale(-1, 1);
						canvas.translate(-canvas.getHeight(), 0);
					}
					else {
						canvas.translate(0, -canvas.getWidth());
					}
					
					drawWidth = canvas.getHeight();
					drawHeight = canvas.getWidth();
					break;
				}
				case 180: {
					canvas.rotate(180);
					if (facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
						canvas.scale(1, -1);
						canvas.translate(-canvas.getWidth(), 0);
					}
					else {
						canvas.translate(-canvas.getWidth(), -canvas.getHeight());
					}
					break;
				}
				case 270: {
					canvas.rotate(270);
					if (facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
						canvas.scale(-1, 1);
					}
					else {
						canvas.translate(-canvas.getHeight(), 0);
					}
					
					drawWidth = canvas.getHeight();
					drawHeight = canvas.getWidth();
					break;
				}
				default:
					if (facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
						canvas.scale(1, -1);
						canvas.translate(0, -canvas.getHeight());
					}
					break;
				}
				
				Rect rect;
				if (mCameraPreviewGravity == Constant.PREVIEW_GRAVITY_RESIZE_ASPECT_FILL) {
					rect = Core.makeAspectFillRect(new Size(frame.getWidth(), frame.getHeight()),
													new Rect(0, 0, drawWidth, drawHeight));
				}
				else {
					rect = Core.makeAspectFitRect(new Size(frame.getWidth(), frame.getHeight()),
													new Rect(0, 0, drawWidth, drawHeight));
				}
				Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
				Bitmap bitmap = frame.getBitmap();
				canvas.drawBitmap(bitmap, 
								new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight()), 
								rect, 
								paint);
				
				canvas.restore();
				
				// 畫fps
				paint.reset();
				paint.setAntiAlias(true);
				paint.setTextSize(25);
				paint.setColor(Color.RED);
				//canvas.drawText(String.format(Locale.US, "FPS:%.1f", fps), 10, 30, paint);
				
				NumberFormat formatter = NumberFormat.getInstance(Locale.US);
				formatter.setMaximumFractionDigits(1);
				formatter.setMinimumFractionDigits(1);
				formatter.setMaximumIntegerDigits(2);
				formatter.setMinimumIntegerDigits(2);
				formatter.setRoundingMode(RoundingMode.HALF_UP); 
				String text = formatter.format(fps);
				canvas.drawText("FPS: "+text
								+" @"+bitmap.getWidth()+"x"+bitmap.getHeight()
								+" Rotation:"+orientation, 10, 30, paint);
				
				holder.unlockCanvasAndPost(canvas);
			} catch (Exception e) {
				e.printStackTrace();
				
				if (canvas != null) {
					holder.unlockCanvasAndPost(canvas);
				}
			}
		}
	}
	
	private class PreviewFrame {
		private int mWidth = 0;
		private int mHeight = 0;
		private Bitmap mBitmap = null;

		public PreviewFrame() {
			super();
		}
		
		@SuppressWarnings("unused")
		public PreviewFrame(int width, int height) {
			super();
			setSize(width, height);
		}
		
		@Override
		protected void finalize() throws Throwable {
			
			if (mBitmap != null) {
				if (!mBitmap.isRecycled()) {
					mBitmap.recycle();
				}
			}
			mBitmap = null;
			
			super.finalize();
		}

		public void setSize(int width, int height) {
			this.mWidth = width;
			this.mHeight = height;
			
			if (mBitmap == null) {
				mBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
			}
			else if (width != mBitmap.getWidth() || height != mBitmap.getHeight()) {
				if (!mBitmap.isRecycled()) {
					mBitmap.recycle();
				}
				mBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
			}
			
		}
		
		public int getWidth() {
			return mWidth;
		}
		
		public int getHeight() {
			return mHeight;
		}

		public Bitmap getBitmap() {
			return mBitmap;
		}
		
	}
	
	private class CameraWorker implements Runnable {
		
		private boolean isRunning() {
			return (RSCameraView.this.isCameraRunning() && RSCameraView.this.mDrawingThread != null && !RSCameraView.this.mDrawingThread.isInterrupted());
		}
		
        public void run() {
            do {
                synchronized (RSCameraView.this) {
                    try {
                    	RSCameraView.this.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        break;
                    }
                }

                if (isRunning()) {
                	PreviewFrame previewFrame = RSCameraView.this.mPreviewFrame;
					RSCameraView.this.onDeliverAndDrawFrame(previewFrame);
				}
            } while (isRunning());
        }
    }

}
