package com.realtouch.camera;

import android.content.Context;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;


@SuppressWarnings({"deprecation", "unused"})
public abstract class CameraViewBase extends ViewGroup implements ICamera {

    protected Session mSession = null;
    private CameraStateListener mCameraStateListener = null;
    private CameraStateListener mInnerCameraStateListener = new CameraStateListener() {

        @Override
        public void onPresetCamera(Camera camera, Camera.Parameters parameters) {
            CameraViewBase.this.mCameraFacing = CameraViewBase.this.mSession.getCameraFacing();
            CameraViewBase.this.mCameraPreviewOrientation = CameraViewBase.this.mSession.getPreviewOrientation();

            CameraViewBase.this.onPresetCamera(camera, parameters);

            if (CameraViewBase.this.mCameraStateListener != null) {
                CameraViewBase.this.mCameraStateListener.onPresetCamera(camera, parameters);
            }
        }

        @Override
        public void onCameraStopped() {
            CameraViewBase.this.onCameraStopped();

            if (CameraViewBase.this.mCameraStateListener != null) {
                CameraViewBase.this.mCameraStateListener.onCameraStopped();
            }
        }

        @Override
        public void onCameraStarted(Camera camera) {
            CameraViewBase.this.onCameraStarted(camera);

            if (CameraViewBase.this.mCameraStateListener != null) {
                CameraViewBase.this.mCameraStateListener.onCameraStarted(camera);
            }
        }

        @Override
        public void onCameraError(int errorCode, String errorMsg) {
            CameraViewBase.this.onCameraError(errorCode, errorMsg);

            if (CameraViewBase.this.mCameraStateListener != null) {
                CameraViewBase.this.mCameraStateListener.onCameraError(errorCode, errorMsg);
            }
        }
    };

    private OnLayoutChangeListener mOnLayoutChanged = new OnLayoutChangeListener() {
        @Override
        public void onLayoutChange(View view, int nl, int nt, int nr, int nb, int ol, int ot, int or, int ob) {
            CameraViewBase.this.removeOnLayoutChangeListener(CameraViewBase.this.mOnLayoutChanged);
            CameraViewBase.this.onLayoutChanged(view, nl, nt, nr, nb, ol, ot, or, ob);
            CameraViewBase.this.addOnLayoutChangeListener(CameraViewBase.this.mOnLayoutChanged);
        }
    };

    protected Context mContext = null;
    protected int mCameraFacing;
    protected int mCameraPreviewOrientation;

    protected int mCameraPreviewGravity = Constant.PREVIEW_GRAVITY_RESIZE_ASPECT_FIT;

    public CameraViewBase(Context context) {
        this(context, null);
    }

    public CameraViewBase(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CameraViewBase(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        mContext = context;

        mSession = new Session(context);
        mSession.setCameraStateListener(mInnerCameraStateListener);

        addOnLayoutChangeListener(mOnLayoutChanged);
    }

    @Override
    protected void finalize() throws Throwable {

        try {
            mSession = null;
            mCameraStateListener = null;
            mInnerCameraStateListener = null;

            mContext = null;

            removeOnLayoutChangeListener(mOnLayoutChanged);
            mOnLayoutChanged = null;

        } catch (Throwable e) {
            e.printStackTrace();
        }

        super.finalize();
    }

    public Session getSession() {
        return mSession;
    }

    public int getPreviewGravity() {
        return mCameraPreviewGravity;
    }

    public void setPreviewGravity(int previewGravity) {
        this.mCameraPreviewGravity = previewGravity;
    }

    // -- ICamera

    @Override
    public Camera getCamera() {
        return mSession.getCamera();
    }

    @Override
    public CameraStateListener getCameraStateListener() {
        return mCameraStateListener;
    }

    @Override
    public void setCameraStateListener(CameraStateListener listener) {
        this.mCameraStateListener = listener;
    }

    @Override
    public void startCamera() {
        mSession.startCamera();
    }

    @Override
    public void startCamera(int cameraFacing) {
        mSession.startCamera(cameraFacing);
    }

    @Override
    public void stopCamera() {
        mSession.stopCamera();
    }

    @Override
    public boolean isCameraRunning() {
        return mSession.isCameraRunning();
    }

    @Override
    public void startPreview() {
        mSession.startPreview();
    }

    @Override
    public void stopPreview() {
        mSession.stopPreview();
    }

    @Override
    public void setDefaultParameters(Camera.Parameters parameters) {
        mSession.setDefaultParameters(parameters);
    }

    @Override
    public void setCameraFacing(int cameraFacing) {
        mSession.setCameraFacing(cameraFacing);
    }

    @Override
    public int getCameraFacing() {
        return mSession.getCameraFacing();
    }

    @Override
    public void setMaxPreviewSize(Core.Size maxPreviewSize) {
        mSession.setMaxPreviewSize(maxPreviewSize);
    }

    @Override
    public Camera.Size getPreviewSize() {
        return mSession.getPreviewSize();
    }

    @Override
    public void setMaxPictureSize(Core.Size maxPictureSize) {
        mSession.setMaxPictureSize(maxPictureSize);
    }

    @Override
    public Camera.Size getPictureSize() {
        return mSession.getPictureSize();
    }

    @Override
    public void setPreviewOrientation(int orientation) {
        mSession.setPreviewOrientation(orientation);
        mCameraPreviewOrientation = mSession.getPreviewOrientation();
    }

    @Override
    public int getPreviewOrientation() {
        return mSession.getPreviewOrientation();
    }

    @Override
    public List<Camera.Size> getSupportedPreviewSizes() {
        return mSession.getSupportedPreviewSizes();
    }

    @Override
    public List<Camera.Size> getSupportedPictureSizes() {
        return mSession.getSupportedPictureSizes();
    }

    @Override
    public List<Camera.Size> getSupportedVideoSizes() {
        return mSession.getSupportedVideoSizes();
    }

    @Override
    public List<String> getSupportedFlashModes() {
        return mSession.getSupportedFlashModes();
    }

    @Override
    public List<String> getSupportedFocusModes() {
        return mSession.getSupportedFocusModes();
    }

    @Override
    public List<String> getSupportedWhiteBalance() {
        return mSession.getSupportedWhiteBalance();
    }

    @Override
    public List<String> getSupportedAntibanding() {
        return mSession.getSupportedAntibanding();
    }

    @Override
    public List<String> getSupportedColorEffects() {
        return mSession.getSupportedColorEffects();
    }

    @Override
    public List<String> getSupportedSceneModes() {
        return mSession.getSupportedSceneModes();
    }

    // -- Abstract

    protected abstract void onLayoutChanged(View view, int nl, int nt, int nr, int nb, int ol, int ot, int or, int ob);

    protected abstract void onPresetCamera(Camera camera, Camera.Parameters parameters);
    protected abstract void onCameraStarted(Camera camera);
    protected abstract void onCameraStopped();
    protected abstract void onCameraError(int errorCode, String errorMsg);

}
