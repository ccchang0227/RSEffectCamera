package com.realtouch.camera;

import android.content.Context;
import android.graphics.Rect;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import java.io.IOException;


@SuppressWarnings({"deprecation", "unused"})
public class NativeCameraView extends CameraViewBase {

    @SuppressWarnings("unused")
    private boolean mSurfaceCreated = false;
    private SurfaceView mSurfaceView = null;
    private SurfaceHolder.Callback mSurfaceCallback = new SurfaceHolder.Callback() {

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            NativeCameraView.this.stopCamera();

            NativeCameraView.this.mSurfaceCreated = false;
        }

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            NativeCameraView.this.mSurfaceCreated = true;
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        }
    };

    private Camera.Size mPreviewSize = null;

    public NativeCameraView(Context context) {
        this(context, null);
    }

    public NativeCameraView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NativeCameraView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        mSurfaceView = new SurfaceView(context);
        addView(mSurfaceView);
        SurfaceHolder surfaceHolder = mSurfaceView.getHolder();
        surfaceHolder.addCallback(mSurfaceCallback);

    }

    @Override
    protected void finalize() throws Throwable {

        try {
            mPreviewSize = null;

            mSurfaceView = null;
            mSurfaceCallback = null;

        } catch (Throwable e) {
            e.printStackTrace();
        }

        super.finalize();
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        if (changed) {
            onLayoutChanged(this, l, t, r, b, 0, 0, 0, 0);
        }
    }

    @Override
    public void setPreviewGravity(int previewGravity) {
        if (previewGravity != mCameraPreviewGravity) {
            requestLayout();
        }

        super.setPreviewGravity(previewGravity);
    }

    // -- CameraViewBase

    @Override
    protected void onLayoutChanged(View view, int nl, int nt, int nr, int nb, int ol, int ot, int or, int ob) {
        if (view == this && getChildCount() > 0) {
            int previewWidth = 0;
            int previewHeight = 0;
            if (mPreviewSize != null) {
                if (mCameraPreviewOrientation%180 == 90) {
                    previewWidth = mPreviewSize.height;
                    previewHeight = mPreviewSize.width;
                }
                else {
                    previewWidth = mPreviewSize.width;
                    previewHeight = mPreviewSize.height;
                }
            }

            int width = nr - nl;
            int height = nb - nt;

            Rect rect;
            switch (mCameraPreviewGravity) {
                case Constant.PREVIEW_GRAVITY_RESIZE_ASPECT_FILL: {
                    rect = Core.makeAspectFillRect(new Core.Size(previewWidth, previewHeight),
                            new Rect(0, 0, width, height));
                    break;
                }
                case Constant.PREVIEW_GRAVITY_RESIZE_ASPECT_FIT: {
                    rect = Core.makeAspectFitRect(new Core.Size(previewWidth, previewHeight),
                            new Rect(0, 0, width, height));

                    break;
                }
                default: {
                    rect = new Rect(0, 0, width, height);
                    break;
                }
            }

            mSurfaceView.layout(rect.left, rect.top, rect.right, rect.bottom);
        }
    }

    @Override
    protected void onPresetCamera(Camera camera, Camera.Parameters parameters) {
        // 設定預設參數
        mSession.setDefaultParameters(parameters);

        if (mSurfaceCreated) {
            try {
                camera.setPreviewDisplay(mSurfaceView.getHolder());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    protected void onCameraStarted(Camera camera) {
        try {
            mPreviewSize = camera.getParameters().getPreviewSize();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onCameraStopped() {
        mPreviewSize = null;
    }

    @Override
    protected void onCameraError(int errorCode, String errorMsg) {
    }

}
