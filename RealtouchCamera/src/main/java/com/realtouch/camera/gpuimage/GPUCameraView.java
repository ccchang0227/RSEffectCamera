package com.realtouch.camera.gpuimage;

import android.content.Context;
import android.hardware.Camera;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.view.View;

import com.realtouch.camera.CameraViewBase;
import com.realtouch.camera.Constant;
import com.realtouch.camera.Core;

import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.Rotation;

@SuppressWarnings({"deprecation", "unused"})
public class GPUCameraView extends CameraViewBase {

    private GLSurfaceView mGLSurfaceView = null;
    private GPUImage mGPUImage = null;
    private GPUImageFilter mImageFilter = null;
    private GPUImageFilterTools.FilterAdjuster mFilterAdjuster = null;

    public GPUCameraView(Context context) {
        this(context, null);
    }

    public GPUCameraView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GPUCameraView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        mGLSurfaceView = new GLSurfaceView(context);
        addView(mGLSurfaceView);

        mGPUImage = new GPUImage(mContext);
        mGPUImage.setGLSurfaceView(mGLSurfaceView);
        mGPUImage.setBackgroundColor(0.0f, 0.0f, 0.0f);

    }

    @Override
    protected void finalize() throws Throwable {

        try {
            mGPUImage.setGLSurfaceView(null);
            mGPUImage.setFilter(null);
            mGPUImage = null;

            mImageFilter = null;
            mFilterAdjuster = null;

            mGLSurfaceView = null;

        } catch (Throwable e) {
            e.printStackTrace();
        }

        super.finalize();
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        if (changed) {
            onLayoutChanged(this, l, t, r, b, 0, 0, 0, 0);
        }
    }

    public GPUImage getGPUImage() {
        return mGPUImage;
    }

    public GPUImageFilter getFilter() {
        return mImageFilter;
    }

    public void setFilter(GPUImageFilter filter) {
        if (mImageFilter != null &&
                filter != null &&
                mImageFilter.getClass().equals(filter.getClass())) {
            return;
        }

        mImageFilter = filter;
        if (mGPUImage != null) {
            mGPUImage.setFilter(filter);
        }

        if (mImageFilter != null) {
            mFilterAdjuster = new GPUImageFilterTools.FilterAdjuster(mImageFilter);
        }
        else {
            mFilterAdjuster = null;
        }
    }

    public void setFilterValue(int value) {
        if (mFilterAdjuster != null && mFilterAdjuster.canAdjust()) {
            mFilterAdjuster.adjust(value);
        }
    }

    public void setPreviewGravity(int previewGravity) {
        super.setPreviewGravity(previewGravity);

        switch (previewGravity) {
            case Constant.PREVIEW_GRAVITY_RESIZE_ASPECT_FILL:
                mGPUImage.setScaleType(GPUImage.ScaleType.CENTER_CROP);
                break;
            case Constant.PREVIEW_GRAVITY_RESIZE_ASPECT_FIT:
                mGPUImage.setScaleType(GPUImage.ScaleType.CENTER_INSIDE);
            default:
                break;
        }
    }

    // -- ICamera

    @Override
    public void setMaxPreviewSize(Core.Size maxPreviewSize) {
        mGPUImage.deleteImage();

        super.setMaxPreviewSize(maxPreviewSize);
    }

    @Override
    public void setPreviewOrientation(int orientation) {
        super.setPreviewOrientation(orientation);

        int facing = GPUCameraView.this.mCameraFacing;
        boolean flipHorizontal = (mCameraPreviewOrientation % 180 != 90 && facing == Camera.CameraInfo.CAMERA_FACING_FRONT);
        boolean flipVertical = (mCameraPreviewOrientation % 180 == 90 && facing == Camera.CameraInfo.CAMERA_FACING_FRONT);
        mGPUImage.setRotation(Rotation.fromInt(mCameraPreviewOrientation), flipHorizontal, flipVertical);
    }

    // -- CameraViewBase

    @Override
    protected void onLayoutChanged(View view, int nl, int nt, int nr, int nb, int ol, int ot, int or, int ob) {
        if (view == this && getChildCount() > 0) {
            int width = nr - nl;
            int height = nb - nt;
            mGLSurfaceView.layout(0, 0, width, height);
        }
    }

    @Override
    protected void onPresetCamera(Camera camera, Camera.Parameters parameters) {
        // 設定預設參數
        mSession.setDefaultParameters(parameters);

        setPreviewGravity(GPUCameraView.this.mCameraPreviewGravity);
    }

    @Override
    protected void onCameraStarted(Camera camera) {
        int facing = mCameraFacing;
        int orientation = mCameraPreviewOrientation;
        boolean flipHorizontal = (orientation%180 != 90 && facing == Camera.CameraInfo.CAMERA_FACING_FRONT);
        boolean flipVertical = (orientation%180 == 90 && facing == Camera.CameraInfo.CAMERA_FACING_FRONT);
        mGPUImage.setUpCamera(camera, orientation, flipHorizontal, flipVertical);
    }

    @Override
    protected void onCameraStopped() {}

    @Override
    protected void onCameraError(int errorCode, String errorMsg) {}

}
