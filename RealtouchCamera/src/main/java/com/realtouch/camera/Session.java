package com.realtouch.camera;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.os.AsyncTask;
import android.os.Build;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@SuppressWarnings({"deprecation", "StatementWithEmptyBody"})
@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
@SuppressLint("NewAi")
public final class Session implements ICamera {
	@SuppressWarnings({"deprecation", "UnnecessaryInterfaceModifier"})
	public interface PreviewFrameCallback {
		public void onProcessPreview(byte[] yuv420sp, Camera.Size previewSize, int previewFormat);
	}

	private final static String ERROR_MSG_NO_CAMERA_DEVICE_AVAILABLE = "找不到相機";
	private final static String ERROR_MSG_INVALID_CAMERA_FACING = "無法識別相機位置";
	private final static String ERROR_MSG_ERROR_OCCURRED_DURING_OPEN_CAMERA = "開啟相機時發生錯誤";
	private final static String EXCEPTION_MSG_CAMERA_NOT_OPEN = "相機尚未開啟";

	private static final int MAGIC_TEXTURE_ID = 10;
	
	private enum State {
		Ready,
		Start,
		Running,
		Stop
	}
	
	private final Context mContext;
	private State mState = State.Ready;
	
	private int mCameraFacing = Constant.CAMERA_FACING_BACK;
	private CameraStateListener mStateListener = null;
	private Core.Size mMaxPreviewSize = null;
	private Core.Size mMaxPictureSize = null;
	private int mPreviewOrientation = Constant.ORIENTATION_DEVICE;

	private List<Camera.Size> mSupportedPreviewSizes = null;
	private List<Camera.Size> mSupportedPictureSizes = null;
	private List<Camera.Size> mSupportedVideoSizes = null;

	private Camera mCamera = null;
	private int mCurrentCameraID = -1;
	private int mCurrentCameraFacing = -1;
	private OpenCameraTask openCameraTask = null;

//	private CameraManager mCameraManager = null;

	private byte[] mPreviewBuffer = null;
	private SurfaceTexture mSurfaceTexture = null;
	private boolean mIsGettingCameraPreview = false;
	private final PreviewCallback mPreviewCallback = new PreviewCallback() {
		
		@Override
		public void onPreviewFrame(byte[] data, Camera camera) {
			
			if (camera != null && Session.this.mState == State.Running) {
				Camera.Size previewSize = camera.getParameters().getPreviewSize();
				int previewFormat = camera.getParameters().getPreviewFormat();
				if (data != null && data.length > 0) {
					synchronized (Session.this) {
						try {
							Session.this.mPreviewFrameCallback.onProcessPreview(data, previewSize, previewFormat);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
				
				Session.this.checkIfPreviewBufferSizeShouldChange(camera);
				if (Session.this.mPreviewBuffer != null) {
					camera.addCallbackBuffer(Session.this.mPreviewBuffer);
				}
			}
		}
	};
	private PreviewFrameCallback mPreviewFrameCallback = null;
	
	public Session(Context context) {
		super();
		this.mContext = context;
	}
	
	@Override
	protected void finalize() throws Throwable {
		
		synchronized (this) {
			changeState(State.Stop);
			releaseCamera();
		}
		
		super.finalize();
	}
	
	public boolean enablePreviewCallback(PreviewFrameCallback callback) {
		if (mIsGettingCameraPreview) {
			disablePreviewCallback();
		}
		if (mCamera == null) {
			return false;
		}
		
		checkIfPreviewBufferSizeShouldChange(mCamera);
		if (mCamera != null) {
			try {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
					mSurfaceTexture = new SurfaceTexture(MAGIC_TEXTURE_ID);
					mCamera.setPreviewTexture(mSurfaceTexture);
	            }
				else {
					mCamera.setPreviewDisplay(null);
	            }
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			this.mPreviewFrameCallback = callback;
			if (mPreviewBuffer != null) {
				mCamera.addCallbackBuffer(mPreviewBuffer);
				mCamera.setPreviewCallbackWithBuffer(mPreviewCallback);
			}
			else {
				mCamera.setPreviewCallback(mPreviewCallback);
			}
			
	        mIsGettingCameraPreview = true;
		}
		
		return mIsGettingCameraPreview;
	}
	
	public void disablePreviewCallback() {
		mPreviewFrameCallback = null;
		if (mCamera != null) {
			mCamera.setPreviewCallback(null);
		}
		mPreviewBuffer = null;
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB && mSurfaceTexture != null) {
			mSurfaceTexture.release();
        }
		mSurfaceTexture = null;
		
		mIsGettingCameraPreview = false;
	}
	
	/// ICamera

	@Override
	public Camera getCamera() {
		return mCamera;
	}
	
	@Override
	public CameraStateListener getCameraStateListener() {
		return this.mStateListener;
	}
	
	@Override
	public void setCameraStateListener(CameraStateListener listener) {
		this.mStateListener = listener;
	}
	
	@Override
	public void startCamera() {
		startCamera(Constant.CAMERA_FACING_BACK);
	}

	@Override
	public void startCamera(int cameraFacing) {
		checkState(State.Start, cameraFacing);
	}

	@Override
	public void stopCamera() {
		checkState(State.Stop, mCameraFacing);
	}
	
	@Override
	public boolean isCameraRunning() {
		return (mCamera != null && mState == State.Running);
	}

	@Override
	public void startPreview() {
		if (mCamera == null) {
			throw new NullPointerException(EXCEPTION_MSG_CAMERA_NOT_OPEN);
		}

		mCamera.startPreview();
	}

	@Override
	public void stopPreview() {
		if (mCamera == null) {
			throw new NullPointerException(EXCEPTION_MSG_CAMERA_NOT_OPEN);
		}

		mCamera.stopPreview();
	}

	@Override
	public void setDefaultParameters(Camera.Parameters parameters) {
		List<String> supportedFlashModes = parameters.getSupportedFlashModes();
		if (supportedFlashModes != null && supportedFlashModes.contains(Camera.Parameters.FLASH_MODE_AUTO)) {
			parameters.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
		}
		
		List<String> supportedFocusModes = parameters.getSupportedFocusModes();
        if (supportedFocusModes != null && supportedFocusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
        	parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        }
		
        List<String> supportedWhiteBalanceModes = parameters.getSupportedWhiteBalance();
        if (supportedWhiteBalanceModes != null && supportedWhiteBalanceModes.contains(Camera.Parameters.WHITE_BALANCE_AUTO)) {
        	parameters.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_AUTO);
        }
        
        if (parameters.isAutoWhiteBalanceLockSupported()) {
        	parameters.setAutoWhiteBalanceLock(false);
        }
		if (parameters.isAutoExposureLockSupported()) {
			parameters.setAutoExposureLock(false);
		}

		if (parameters.isVideoStabilizationSupported()) {
			parameters.setVideoStabilization(true);
		}
	}
	
	@Override
	public void setCameraFacing(int cameraFacing) {
		if (cameraFacing == mCameraFacing) {
			return;
		}
		
		stopCamera();
		startCamera(cameraFacing);
	}
	
	@Override
	public int getCameraFacing() {
		if (mCurrentCameraFacing != -1) {
			return mCurrentCameraFacing;
		}
		else {
			Camera.CameraInfo info = new Camera.CameraInfo();
			int cameraID = -1;
			int facing = mCameraFacing;
			
			int numberOfCameras = Camera.getNumberOfCameras();
			for (int i = 0; i < numberOfCameras; i ++) {
				Camera.getCameraInfo(i, info);
				if (info.facing == facing) {
					cameraID = i;
					mCurrentCameraFacing = info.facing;
					mCurrentCameraID = i;
					break;
				}
			}
			
			if (cameraID == -1) {
				throw new NullPointerException(ERROR_MSG_INVALID_CAMERA_FACING);
			}
			return facing;
		}
	}
	
	@Override
	public void setMaxPreviewSize(Core.Size maxPreviewSize) {
		this.mMaxPreviewSize = maxPreviewSize;
		
		if (mCamera != null) {
			/* Important!!!
			 * previewSize改變之後，SurfaceTexture必須重新產生&指定給Camera，
			 * 否則在onPreviewFrame的callback裡會取不到正確的buffer，
			 * 因為SurfaceTexture裡的glTexture的size不會自己改變，所以必須重新產生，
			 * SurfaceHolder不確定有沒有這問題。
			 */
			boolean isPreviewing = (mState == State.Running);
			boolean isListeningPreview = (mSurfaceTexture != null);
			PreviewFrameCallback frameCallback = mPreviewFrameCallback;
			if (isListeningPreview) {
				disablePreviewCallback();
			}
			if (isPreviewing) {
				mCamera.stopPreview();
			}
			
			Camera.Parameters parameters = mCamera.getParameters();
			innerSetPreviewSize(parameters, maxPreviewSize);
			try {
				mCamera.setParameters(parameters);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			if (isListeningPreview) {
				enablePreviewCallback(frameCallback);
			}
			if (isPreviewing) {
				mCamera.startPreview();
			}
			
		}
		
	}
	
	@Override
	public Camera.Size getPreviewSize() {
		if (mCamera == null) {
			return null;
		}
		Camera.Parameters parameters = mCamera.getParameters();
		if (parameters == null) {
			return null;
		}
		
		return parameters.getPreviewSize();
	}
	
	@Override
	public void setMaxPictureSize(Core.Size maxPictureSize) {
		this.mMaxPictureSize = maxPictureSize;
	}
	
	@Override
	public Camera.Size getPictureSize() {
		if (mCamera == null) {
			return null;
		}
		Camera.Parameters parameters = mCamera.getParameters();
		if (parameters == null) {
			return null;
		}
		
		return parameters.getPictureSize();
	}
	
	public void setPreviewOrientation(int orientation) {
		this.mPreviewOrientation = orientation;
		
		if (mCamera != null) {
			if (orientation != Constant.ORIENTATION_DEVICE) {
				orientation = orientation / 90 * 90;
				mCamera.setDisplayOrientation(orientation%360);
			}
			else {
				orientation = Core.getPreferredPreviewOrientation(mContext, mCurrentCameraID);
				mCamera.setDisplayOrientation(orientation);
			}
		}
	}
	
	public int getPreviewOrientation() {
		if (mPreviewOrientation != Constant.ORIENTATION_DEVICE) {
			return mPreviewOrientation%360;
		}
		
		getCameraFacing();
		return Core.getPreferredPreviewOrientation(mContext, mCurrentCameraID);
	}
	
	public List<Camera.Size> getSupportedPreviewSizes() {
		return this.mSupportedPreviewSizes;
	}
	
	public List<Camera.Size> getSupportedPictureSizes() {
		return this.mSupportedPictureSizes;
	}
	
	public List<Camera.Size> getSupportedVideoSizes() {
		return this.mSupportedVideoSizes;
	}
	
	public List<String> getSupportedFlashModes() {
		if (mCamera == null) {
			return null;
		}
		Camera.Parameters parameters = mCamera.getParameters();
		if (parameters == null) {
			return null;
		}
		
		return parameters.getSupportedFlashModes();
	}
	
	public List<String> getSupportedFocusModes() {
		if (mCamera == null) {
			return null;
		}
		Camera.Parameters parameters = mCamera.getParameters();
		if (parameters == null) {
			return null;
		}
		
		return parameters.getSupportedFocusModes();
	}
	
	public List<String> getSupportedWhiteBalance() {
		if (mCamera == null) {
			return null;
		}
		Camera.Parameters parameters = mCamera.getParameters();
		if (parameters == null) {
			return null;
		}
		
		return parameters.getSupportedWhiteBalance();
	}
	
	public List<String> getSupportedAntibanding() {
		if (mCamera == null) {
			return null;
		}
		Camera.Parameters parameters = mCamera.getParameters();
		if (parameters == null) {
			return null;
		}
		
		return parameters.getSupportedAntibanding();
	}
	
	public List<String> getSupportedColorEffects() {
		if (mCamera == null) {
			return null;
		}
		Camera.Parameters parameters = mCamera.getParameters();
		if (parameters == null) {
			return null;
		}
		
		return parameters.getSupportedColorEffects();
	}
	
	public List<String> getSupportedSceneModes() {
		if (mCamera == null) {
			return null;
		}
		Camera.Parameters parameters = mCamera.getParameters();
		if (parameters == null) {
			return null;
		}
		
		return parameters.getSupportedSceneModes();
	}
	
	/// -- Private --
	
	/** 判斷狀態轉跳 */
	private void checkState(State newState, int cameraFacing) {
		if (mState == newState) {
			return;
		}
		
		if (mState == State.Ready) {
			if (newState == State.Start) {
				changeState(newState);
				initializeCamera(cameraFacing);
			}
			else if (newState == State.Running) {
				// 不合邏輯的狀態轉跳，直接返回
			}
			else if (newState == State.Stop) {
				// 不合邏輯的狀態轉跳，直接返回
			}
		}
		else if (mState == State.Start) {
			if (newState == State.Running) {
				// Start->Running由開啟相機的Task管理，直接返回
			}
			else if (newState == State.Stop) {
				// 相機開啟中，把狀態改成Stop，Task結束後自動關閉相機
				changeState(newState);
				if (openCameraTask != null && 
						openCameraTask.getStatus() == AsyncTask.Status.RUNNING) {
					openCameraTask.cancel(true);
				}
			}
			else if (newState == State.Ready) {
				// 不合邏輯的狀態轉跳，直接返回
			}
		}
		else if (mState == State.Running) {
			if (newState == State.Stop) {
				changeState(newState);
				releaseCamera();
			}
			else if (newState == State.Ready || 
					newState == State.Start) {
				// 不合邏輯的狀態轉跳，直接返回
			}
		}
		else if (mState == State.Stop) {
			if (newState == State.Start) {
				//FIXME: 相機停止中時，偵測到要請求開啟相機時該怎麼處理 (目前只想到wait...)
				
			}
			else {
				// 不合邏輯的狀態轉跳，直接返回
			}
		}
		
	}
	
	/** 實際狀態改變 */
	private void changeState(State newState) {
		if (mState == newState) {
			return;
		}
		mState = newState;
	}
	
	/** 開啟&初始化相機 */
	private void initializeCamera(int cameraFacing) {
		if (Camera.getNumberOfCameras() == 0) {
			changeState(State.Ready);
			onCameraError(Constant.ERROR_CODE_DEVICE_NOT_AVAILABLE, ERROR_MSG_NO_CAMERA_DEVICE_AVAILABLE);
			return;
		}
		if (cameraFacing != Constant.CAMERA_FACING_BACK &&
				cameraFacing != Constant.CAMERA_FACING_FRONT) {
			changeState(State.Ready);
			onCameraError(Constant.ERROR_CODE_FAILED_TO_OPEN, ERROR_MSG_INVALID_CAMERA_FACING);
			return;
		}
		synchronized (this) {
			if (openCameraTask != null) {
				switch (openCameraTask.getStatus()) {
				case RUNNING:
					// 相機開啟中，直接返回
					return;
				case FINISHED:
					// 理論上不應該發生
					openCameraTask = null;
					break;
				default: // PENDING
					openCameraTask = null;
					break;
				}
			}
			
			mSupportedPreviewSizes = null;
			mSupportedPictureSizes = null;
			mSupportedVideoSizes = null;
			
			mCameraFacing = cameraFacing;
			openCameraTask = new OpenCameraTask(mCameraFacing);
			openCameraTask.execute();
		}
	}
	
	/** 結束相機 */
	private void releaseCamera() {
		synchronized (this) {
			if (mState != State.Stop) {
				return;
			}
			
			disablePreviewCallback();
			
            if (mCamera != null) {
                mCamera.stopPreview();
                mCamera.setPreviewCallback(null);

                mCamera.release();
            }
            mCamera = null;
            
            mCurrentCameraID = -1;
            mCurrentCameraFacing = -1;
            
            mSupportedPreviewSizes = null;
			mSupportedPictureSizes = null;
			mSupportedVideoSizes = null;
            
			mPreviewBuffer = null;
			
            changeState(State.Ready);
            
            onCameraStopped();
        }
	}
	
	/** 檢查Preview buffer的size */
	private void checkIfPreviewBufferSizeShouldChange(Camera camera) {
		if (camera == null) {
			return;
		}
		Camera.Parameters parameters = camera.getParameters();
		if (parameters == null) {
			return;
		}
		
		Camera.Size previewSize = parameters.getPreviewSize();
		int previewFormat = parameters.getPreviewFormat();
		int size = previewSize.width * previewSize.height;
        size *= (ImageFormat.getBitsPerPixel(previewFormat) / 8f);
        
        if (mPreviewBuffer == null || size != mPreviewBuffer.length) {
        	mPreviewBuffer = new byte[size];
        }
        
	}
	
	/** 相機啟動成功 */
	private void onCameraStarted(Camera camera) {
		if (mStateListener != null) {
			mStateListener.onCameraStarted(camera);
		}
	}
	
	/** 設定相機參數 (在相機preview開啟前) */
	private void onPresetCamera(Camera camera, Camera.Parameters parameters) {
		if (parameters != null) {
			parameters.setPreviewFormat(ImageFormat.NV21);
			parameters.setPictureFormat(ImageFormat.JPEG);
			
			mSupportedPreviewSizes = parameters.getSupportedPreviewSizes();
			mSupportedPictureSizes = parameters.getSupportedPictureSizes();
			mSupportedVideoSizes = parameters.getSupportedVideoSizes();
		}
		else {
			mSupportedPreviewSizes = null;
			mSupportedPictureSizes = null;
			mSupportedVideoSizes = null;
		}
		
		if (mStateListener != null) {
			mStateListener.onPresetCamera(camera, parameters);
		}
	}
	
	private void onCameraStopped() {
		if (mStateListener != null) {
			mStateListener.onCameraStopped();
		}
	}
	
	private void onCameraError(int errorCode, String errorMsg) {
		if (mStateListener != null) {
			mStateListener.onCameraError(errorCode, errorMsg);
		}
	}
	
	private static Camera.Size estimatePreviewSize(Camera camera, Core.Size maxPreviewSize) {
		if (maxPreviewSize == null) {
			return null;
		}
		if (camera == null) {
			return null;
		}
		Camera.Parameters parameters = camera.getParameters();
		if (parameters == null) {
			return null;
		}
		List<Camera.Size> previewSizes = parameters.getSupportedPreviewSizes();
		if (previewSizes == null) {
			return null;
		}
		
		// Sort by size, descending
		Collections.sort(previewSizes, new Comparator<Camera.Size>() {
			@Override
			public int compare(Camera.Size a, Camera.Size b) {
				int aPixels = a.height*a.width;
		        int bPixels = b.height*b.width;
		        if (bPixels < aPixels) {
		          return -1;
		        }
		        else if (bPixels > aPixels) {
		          return 1;
		        }
		        
		        return 0;
		      }
		});
		
		int targetWidth = Math.max(maxPreviewSize.width, maxPreviewSize.height);
		int targetHeight = Math.min(maxPreviewSize.width, maxPreviewSize.height);
		
		Camera.Size previewSize = parameters.getPreviewSize();
		for (Camera.Size size : previewSizes) {
			int width = Math.max(size.width, size.height);
			int height = Math.min(size.width, size.height);
			if (width <= targetWidth && height <= targetHeight) {
				previewSize = size;
				break;
			}
		}
		
		return previewSize;
	}
	
	private void innerSetPreviewSize(Camera.Parameters parameters, Core.Size maxPreviewSize) {
		if (mCamera != null) {
			Camera.Size previewSize = estimatePreviewSize(mCamera, maxPreviewSize);
			if (previewSize != null && parameters != null) {
				try {
					parameters.setPreviewSize(previewSize.width, previewSize.height);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	@SuppressWarnings("deprecation")
	private class OpenCameraTask extends AsyncTask<Void, Void, Camera> {
		private final int mCameraFacing;
		
		OpenCameraTask(int cameraFacing) {
			super();
			
			this.mCameraFacing = cameraFacing;
		}
		
		@Override
		protected void onPreExecute() {
			
			synchronized (Session.this) {
				if (Session.this.mCamera != null) {
					Session.this.mCamera.release();
				}
				Session.this.mCamera = null;
				Session.this.mCurrentCameraID = -1;
				Session.this.mCurrentCameraFacing = -1;
			}
			
			super.onPreExecute();
		}
		
		@Override
		protected Camera doInBackground(Void... params) {
			
			Camera camera = null;
			try {
//				Thread.sleep(300);
//				if (isCancelled()) {
//					return null;
//				}
				
				Camera.CameraInfo info = new Camera.CameraInfo();
				int cameraID = 0;
				int facing = mCameraFacing;

				int numberOfCameras = Camera.getNumberOfCameras();
				for (int i = 0; i < numberOfCameras; i ++) {
					Camera.getCameraInfo(i, info);
					if (info.facing == facing) {
						cameraID = i;
						break;
					}
				}
				
				if (isCancelled()) {
					return null;
				}
				
				if (cameraID >= 0 &&
						cameraID < numberOfCameras) {
					camera = Camera.open(cameraID);
				}
				
				if (camera == null) {
					throw new IOException(ERROR_MSG_NO_CAMERA_DEVICE_AVAILABLE);
				}
				
				if (isCancelled()) {
					camera.release();
					camera = null;
					return null;
				}

				Camera.getCameraInfo(cameraID, info);
				synchronized (Session.this) {
                    Session.this.mCurrentCameraFacing = info.facing;
                    Session.this.mCurrentCameraID = cameraID;
                }

			} catch (Throwable e) {
				e.printStackTrace();
				
				if (camera != null) {
					camera.release();
				}
				camera = null;
			}
			
			return camera;
		}
		
		@Override
		protected void onPostExecute(Camera result) {
			if (isCancelled() || result == null) {
				if (result != null) {
					result.release();
				}

				Session.this.onCameraError(Constant.ERROR_CODE_FAILED_TO_OPEN, Session.ERROR_MSG_ERROR_OCCURRED_DURING_OPEN_CAMERA);

				super.onPostExecute(null);
				return;
			}

			if (Session.this.mState == State.Start) {
                Session.this.mCamera = result;

                Camera.Parameters parameters = Session.this.mCamera.getParameters();
                Session.this.onPresetCamera(Session.this.mCamera, parameters);

                // 設定 preview size
                Session.this.innerSetPreviewSize(parameters, Session.this.mMaxPreviewSize);
                // 設定 picture size
                Session.this.setMaxPictureSize(Session.this.mMaxPictureSize);
                // 設定 preview 轉向
                Session.this.setPreviewOrientation(Session.this.mPreviewOrientation);

                if (parameters != null) {
                    Session.this.mCamera.setParameters(parameters);
                }

                Session.this.mCamera.startPreview();
                Session.this.changeState(State.Running);
                Session.this.onCameraStarted(Session.this.mCamera);
            }
            else {
                Session.this.mCamera = result;
                Session.this.changeState(State.Stop);
                Session.this.releaseCamera();
            }
			Session.this.openCameraTask = null;

			super.onPostExecute(result);
		}
		
		@Override
		protected void onCancelled() {

			Session.this.changeState(State.Stop);
			Session.this.releaseCamera();
			Session.this.openCameraTask = null;

			super.onCancelled();
		}
		
	}
	
}
