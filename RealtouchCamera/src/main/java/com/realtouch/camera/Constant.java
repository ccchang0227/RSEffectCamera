package com.realtouch.camera;

import android.hardware.Camera;

@SuppressWarnings({"deprecation", "unused"})
public final class Constant {
	/** 預覽畫面等比例顯示 */
	public static final int PREVIEW_GRAVITY_RESIZE_ASPECT_FIT = 1;
	/** 預覽畫面等比例填滿 */
	public static final int PREVIEW_GRAVITY_RESIZE_ASPECT_FILL = 2;

	/** 後鏡頭 */
	public static final int CAMERA_FACING_BACK = Camera.CameraInfo.CAMERA_FACING_BACK;
	/** 前鏡頭 */
	public static final int CAMERA_FACING_FRONT = Camera.CameraInfo.CAMERA_FACING_FRONT;
	
	/** 預覽畫面/照片轉向-根據裝置 */
	public static final int ORIENTATION_DEVICE = -1;
	/** 預覽畫面/照片轉向-0度 */
	public static final int ORIENTATION_0 = 0;
	/** 預覽畫面/照片轉向-90度 */
	public static final int ORIENTATION_90 = 90;
	/** 預覽畫面/照片轉向-180度 */
	public static final int ORIENTATION_180 = 180;
	/** 預覽畫面/照片轉向-270度 */
	public static final int ORIENTATION_270 = 270;
	
	/** 照片進行鏡像處理 */
	public static final int MIRRORED_TRUE = 1;
	/** 照片不進行鏡像處理 */
	public static final int MIRRORED_FALSE = 0;
	
	public static final int MIN_PREVIEW_PIXELS = 470 * 320; // normal screen
	public static final int MAX_PREVIEW_PIXELS = 1280 * 720;
	
	// -- error code

	public static final int ERROR_CODE_DEVICE_NOT_AVAILABLE = -2011;
	public static final int ERROR_CODE_FAILED_TO_OPEN = -2012;
	public static final int ERROR_CODE_OTHER_FAILURE = -3000;
}
