#pragma version(1)
#pragma rs java_package_name(com.realtouch.image.rs)
#pragma rs_fp_relaxed

#include "Common.rsh"

/** 亮度權重 */
float brightStrength = 60.0;

/** 平均RGB值 */
static float3 aver = {0, 0, 0};

/** Input */
static rs_allocation inAlloc;
/** Output */
static rs_allocation outAlloc;

/** default kernel */
void root(const uchar4 *in, uchar4 *out, uint32_t x, uint32_t y) {
	rs_element inElement = rsAllocationGetElement(inAlloc);
	rs_element outElement = rsAllocationGetElement(outAlloc);
	
	rs_data_type inDataType = rsElementGetDataType(inElement);
	rs_data_type outDataType = rsElementGetDataType(outElement);
	
	float strength = clamp(brightStrength, 60.0, 248.0);
	
	float sr = 1.0, sg = 1.0, sb = 1.0;
 	float br = 0.0, bg = 0.0, bb = 0.0;
 	
 	float diff = 0;
	if (strength-aver.r > diff) diff = strength-aver.r;
	if (strength-aver.g > diff) diff = strength-aver.g;
	if (strength-aver.b > diff) diff = strength-aver.b;
	if (diff > 0) {
		diff = diff*3/(aver.r+aver.g+aver.b);
	
		sr += diff;
		sg += diff;
		sb += diff;
	}
	
	float4 f4;
	if (inDataType == RS_TYPE_UNSIGNED_5_6_5) { //RGB565
		ushort rgb565 = rsGetElementAt_ushort(inAlloc, x, y);
		f4 = getFloatRGB_565(rgb565); // [0-1]
	}
	else { // ARGB8888
		uchar4 c4 = rsGetElementAt_uchar4(inAlloc, x, y); // [0-255]
		f4 = getFloatARGB_8888(c4); // [0-1]
	}
    float r = f4.r*255.0*sr + br;
    float g = f4.g*255.0*sg + bg;
    float b = f4.b*255.0*sb + bb;
    r = clamp(r, 0.0f, 255.0f);
    g = clamp(g, 0.0f, 255.0f);
    b = clamp(b, 0.0f, 255.0f);
	
	r /= 255.0;
	g /= 255.0;
	b /= 255.0;
	
	f4.r = r;
	f4.g = g;
	f4.b = b;
	if (outDataType == RS_TYPE_UNSIGNED_5_6_5) { //RGB565
		ushort rgb565 = getUShortRGB_565(f4);
		rsSetElementAt_ushort(outAlloc, rgb565, x, y);
	}
	else { // ARGB8888
		uchar4 c4 = getUCharARGB_8888(f4);
		rsSetElementAt_uchar4(outAlloc, c4, x, y);
	}
}

/** 設定input */
void setInput(rs_allocation input) {
	inAlloc = input;
	
	rs_element inElement = rsAllocationGetElement(inAlloc);
	rs_data_type inDataType = rsElementGetDataType(inElement);
	
	uint32_t dimInX = rsAllocationGetDimX(inAlloc);
	uint32_t dimInY = rsAllocationGetDimY(inAlloc);
	
	float aver_r = 0.0;
	float aver_g = 0.0;
	float aver_b = 0.0;
	for (int i=0; i<dimInX; i++) {
		for (int j=0; j<dimInY; j++) {
			float4 f4;
			if (inDataType == RS_TYPE_UNSIGNED_5_6_5) { //RGB565
				ushort rgb565 = rsGetElementAt_ushort(inAlloc, i, j);
				f4 = getFloatRGB_565(rgb565); // [0-1]
			}
			else { // ARGB8888
				uchar4 c4 = rsGetElementAt_uchar4(inAlloc, i, j); // [0-255]
				f4 = getFloatARGB_8888(c4); // [0-1]
			}
			float r = clamp(f4.r, (60.0/255.0), (248.0/255.0));
		    float g = clamp(f4.g, (60.0/255.0), (248.0/255.0));
		    float b = clamp(f4.b, (60.0/255.0), (248.0/255.0));
			aver_r += (r*255.0);
			aver_g += (g*255.0);
			aver_b += (b*255.0);
		}
	}
	aver_r /= (dimInX*dimInY);
	aver_g /= (dimInX*dimInY);
	aver_b /= (dimInX*dimInY);
	
	aver.r = aver_r;
	aver.g = aver_g;
	aver.b = aver_b;
}

/** 調整亮度 */
void adjustBright(rs_script script, rs_allocation output) {
	outAlloc = output;
	rsForEach(script, output, output);
}

/** 調亮權重 */
float brightWeight = 1.0;

/** 用renderscript的kernel function調整飽和度(foreach) - RGBA8888 */
uchar4 __attribute__((kernel)) simple_bright_rgba8888(uchar4 in) {
	float4 f4 = getFloatARGB_8888(in);
	
	f4.rgb = f4.rgb*brightWeight;
	f4.rgb = clamp(f4.rgb, 0, 1.0f);
	
	return getUCharARGB_8888(f4);
}
