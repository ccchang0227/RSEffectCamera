#pragma version(1)
#pragma rs java_package_name(com.realtouch.image.rs)
#pragma rs_fp_relaxed

#include "Common.rsh"

/** 飽和度權重 */
float saturationWeight = 0.05;

/** Input */
static rs_allocation inAlloc;
/** Output */
static rs_allocation outAlloc;

/** default kernel */
void root(const uchar4 *in, uchar4 *out, uint32_t x, uint32_t y) {
	float weight = clamp(saturationWeight, 0.0, 1.0);
	float logWeight = log(weight);
	
	rs_element inElement = rsAllocationGetElement(inAlloc);
	rs_element outElement = rsAllocationGetElement(outAlloc);
	
	rs_data_type inDataType = rsElementGetDataType(inElement);
	rs_data_type outDataType = rsElementGetDataType(outElement);
	
	float4 f4;
	if (inDataType == RS_TYPE_UNSIGNED_5_6_5) { //RGB565
		ushort rgb565 = rsGetElementAt_ushort(inAlloc, x, y);
		f4 = getFloatRGB_565(rgb565); // [0-1]
	}
	else { // ARGB8888
		uchar4 c4 = rsGetElementAt_uchar4(inAlloc, x, y); // [0-255]
		f4 = getFloatARGB_8888(c4); // [0-1]
	}
	
    HSV hsv = getHSVFrom(f4);
	float s = hsv.s;
	if (s != 0.0) {
		s = s * (log(s)/logWeight);
	}
	hsv.s = hsv.s + s;
	hsv.s = clamp(hsv.s, 0.0, 1.0);
	f4 = getRGBAFrom(hsv);
	
	if (outDataType == RS_TYPE_UNSIGNED_5_6_5) { //RGB565
		ushort rgb565 = getUShortRGB_565(f4);
		rsSetElementAt_ushort(outAlloc, rgb565, x, y);
	}
	else { // ARGB8888
		uchar4 c4 = getUCharARGB_8888(f4);
		rsSetElementAt_uchar4(outAlloc, c4, x, y);
	}
}

/** 調整飽和度 */
void adjustSaturation(rs_script script, rs_allocation input, rs_allocation output) {
	inAlloc = input;
	outAlloc = output;
	rsForEach(script, output, output);
}

float change = 1.0f;
static float3 P_rgb = {0.299f, 0.587f, 0.114f};
uchar4 __attribute__((kernel)) simple_saturation(uchar4 in) {
	float4 f4 = getFloatARGB_8888(in);
	
	double P = sqrt(f4.r*f4.r*P_rgb.r+f4.g*f4.g*P_rgb.g+f4.b*f4.b*P_rgb.b);
	
	f4.rgb = P + (f4.rgb-P)*change;
	f4.rgb = clamp(f4.rgb, 0.0f, 1.0f);
	
    return getUCharARGB_8888(f4);
}
