
#pragma version(1)
#pragma rs java_package_name(com.realtouch.image.rs)
#pragma rs_fp_relaxed

#include "Common.rsh"

/** 亮度權重 */
float brightStrength = 60.0;
/** 飽和度權重 */
float saturationWeight = 0.05;

rs_allocation input;

/** 調亮 (僅支援 4.3.1以上) */ 
void bright(rs_allocation in, rs_allocation out) {
	rs_element inElement = rsAllocationGetElement(in);
	rs_element outElement = rsAllocationGetElement(out);
	
	rs_data_type inDataType = rsElementGetDataType(inElement);
	rs_data_type outDataType = rsElementGetDataType(outElement);
	//rsDebug("dataType-in:%d, out:%d", inDataType, outDataType);
	
	uint32_t dimInX = rsAllocationGetDimX(in);
	uint32_t dimInY = rsAllocationGetDimY(in);
	
	float strength = clamp(brightStrength, 60.0, 248.0);
	
	float sr = 1.0, sg = 1.0, sb = 1.0;
 	float br = 0.0, bg = 0.0, bb = 0.0;
	float aver_r = 0.0;
	float aver_g = 0.0;
	float aver_b = 0.0;
	for (int i=0; i<dimInX; i++) {
		for (int j=0; j<dimInY; j++) {
			float4 f4;
			if (inDataType == RS_TYPE_UNSIGNED_5_6_5) { //RGB565
				ushort rgb565 = rsGetElementAt_ushort(in, i, j);
				f4 = getFloatRGB_565(rgb565); // [0-1]
			}
			else { // ARGB8888
				uchar4 c4 = rsGetElementAt_uchar4(in, i, j); // [0-255]
				f4 = getFloatARGB_8888(c4); // [0-1]
			}
			float r = clamp(f4.r, (60.0/255.0), (248.0/255.0));
		    float g = clamp(f4.g, (60.0/255.0), (248.0/255.0));
		    float b = clamp(f4.b, (60.0/255.0), (248.0/255.0));
			aver_r += (r*255.0);
			aver_g += (g*255.0);
			aver_b += (b*255.0);
		}
	}
	aver_r /= (dimInX*dimInY);
	aver_g /= (dimInX*dimInY);
	aver_b /= (dimInX*dimInY);
	
	//rsDebug("aver_r=%f, aver_g=%f, aver_b=%f", aver_r, aver_g, aver_b);
	
	float diff = 0;
	if (strength-aver_r > diff) diff = strength-aver_r;
	if (strength-aver_g > diff) diff = strength-aver_g;
	if (strength-aver_b > diff) diff = strength-aver_b;
	if (diff > 0) {
		diff = diff*3/(aver_r+aver_g+aver_b);
	
		sr += diff;
		sg += diff;
		sb += diff;
	}
	
	uint32_t dimOutX = rsAllocationGetDimX(out);
	uint32_t dimOutY = rsAllocationGetDimY(out);
	for (int i=0; i<dimOutX; i++) {
		for (int j=0; j<dimOutY; j++) {
			if (i >= dimInX || j >= dimInY) {
				if (outDataType == RS_TYPE_UNSIGNED_5_6_5) { //RGB565
					ushort rgb565 = 0;
					rsSetElementAt_ushort(out, rgb565, i, j);
				}
				else { // ARGB8888
					uchar4 c4 = rsPackColorTo8888(0.0, 0.0, 0.0, 1.0); // [0-255]
					rsSetElementAt_uchar4(out, c4, i, j);
				}
				continue;
			}
			
			float4 f4;
			if (inDataType == RS_TYPE_UNSIGNED_5_6_5) { //RGB565
				ushort rgb565 = rsGetElementAt_ushort(in, i, j);
				f4 = getFloatRGB_565(rgb565); // [0-1]
			}
			else { // ARGB8888
				uchar4 c4 = rsGetElementAt_uchar4(in, i, j); // [0-255]
				f4 = getFloatARGB_8888(c4); // [0-1]
			}
		    float r = f4.r*255.0*sr + br;
		    float g = f4.g*255.0*sg + bg;
		    float b = f4.b*255.0*sb + bb;
		    r = clamp(r, 0.0f, 255.0f);
		    g = clamp(g, 0.0f, 255.0f);
		    b = clamp(b, 0.0f, 255.0f);
			
			r /= 255.0;
			g /= 255.0;
			b /= 255.0;
			
			f4.r = r;
			f4.g = g;
			f4.b = b;
			if (outDataType == RS_TYPE_UNSIGNED_5_6_5) { //RGB565
				ushort rgb565 = getUShortRGB_565(f4);
				rsSetElementAt_ushort(out, rgb565, i, j);
			}
			else { // ARGB8888
				uchar4 c4 = getUCharARGB_8888(f4);
				rsSetElementAt_uchar4(out, c4, i, j);
			}
		}
	}
	//rsDebug("sr=%f, sg=%f, sb=%f", sr, sg, sb);
	//rsDebug("In dimInX=%d, dimInY=%d", dimInX, dimInY);
}

/** 調飽和度 (僅支援 4.3.1以上) */
void saturation(rs_allocation in, rs_allocation out) {
	rs_element inElement = rsAllocationGetElement(in);
	rs_element outElement = rsAllocationGetElement(out);
	
	rs_data_type inDataType = rsElementGetDataType(inElement);
	rs_data_type outDataType = rsElementGetDataType(outElement);
	//rsDebug("dataType-in:%d, out:%d", inDataType, outDataType);
	
	uint32_t dimInX = rsAllocationGetDimX(in);
	uint32_t dimInY = rsAllocationGetDimY(in);
	
	float weight = clamp(saturationWeight, 0.0, 1.0);
	float logWeight = log(weight);
	
	uint32_t dimOutX = rsAllocationGetDimX(out);
	uint32_t dimOutY = rsAllocationGetDimY(out);
	for (int i=0; i<dimOutX; i++) {
		for (int j=0; j<dimOutY; j++) {
			if (i >= dimInX || j >= dimInY) {
				if (outDataType == RS_TYPE_UNSIGNED_5_6_5) { //RGB565
					ushort rgb565 = 0;
					rsSetElementAt_ushort(out, rgb565, i, j);
				}
				else { // ARGB8888
					uchar4 c4 = rsPackColorTo8888(0.0, 0.0, 0.0, 1.0); // [0-255]
					rsSetElementAt_uchar4(out, c4, i, j);
				}
				continue;
			}
			
			float4 f4;
			if (inDataType == RS_TYPE_UNSIGNED_5_6_5) { //RGB565
				ushort rgb565 = rsGetElementAt_ushort(in, i, j);
				f4 = getFloatRGB_565(rgb565); // [0-1]
			}
			else { // ARGB8888
				uchar4 c4 = rsGetElementAt_uchar4(in, i, j); // [0-255]
				f4 = getFloatARGB_8888(c4); // [0-1]
			}
			
			HSV hsv = getHSVFrom(f4);
			float s = hsv.s;
    		if (s != 0.0) {
    			s = s * (log(s)/logWeight);
    		}
    		hsv.s = hsv.s + s;
    		hsv.s = clamp(hsv.s, 0.0, 1.0);
			f4 = getRGBAFrom(hsv);
			
			if (outDataType == RS_TYPE_UNSIGNED_5_6_5) { //RGB565
				ushort rgb565 = getUShortRGB_565(f4);
				rsSetElementAt_ushort(out, rgb565, i, j);
			}
			else { // ARGB8888
				uchar4 c4 = getUCharARGB_8888(f4);
				rsSetElementAt_uchar4(out, c4, i, j);
			}
		}
	}
}

/** 用renderscript的kernel function調整飽和度(foreach) - RGBA8888 */
uchar4 __attribute__((kernel)) saturation_rgba8888(uchar4 in) {
	float weight = clamp(saturationWeight, 0.0, 1.0);
	float logWeight = log(weight);
	
	float4 f4 = getFloatARGB_8888(in);
	HSV hsv = getHSVFrom(f4);
	float s = hsv.s;
	if (s != 0.0) {
		s = s * (log(s)/logWeight);
	}
	hsv.s = hsv.s + s;
	hsv.s = clamp(hsv.s, 0.0, 1.0);
	f4 = getRGBAFrom(hsv);
	
	return getUCharARGB_8888(f4);
}
/** 用renderscript的kernel function調整飽和度(foreach) - RGB565 */
ushort __attribute__((kernel)) saturation_rgb565(ushort in) {
	float weight = clamp(saturationWeight, 0.0, 1.0);
	float logWeight = log(weight);
	
	float4 f4 = getFloatRGB_565(in);
	HSV hsv = getHSVFrom(f4);
	float s = hsv.s;
	if (s != 0.0) {
		s = s * (log(s)/logWeight);
	}
	hsv.s = hsv.s + s;
	hsv.s = clamp(hsv.s, 0.0, 1.0);
	f4 = getRGBAFrom(hsv);
	
	return getUShortRGB_565(f4);
}

const static float3 gMonoMult = {0.299f, 0.587f, 0.114f};
/** 灰階 - RGBA8888 */
uchar4 __attribute__((kernel)) gray_rgba8888(uchar4 in) {
	float4 f4 = getFloatARGB_8888(in);
	float3 result = dot(f4.rgb, gMonoMult);
	f4.rgb = result.rgb;
    return getUCharARGB_8888(f4);
}
/** 灰階 - RGB565 */
ushort __attribute__((kernel)) gray_rgb565(ushort in) {
	float4 f4 = getFloatRGB_565(in);
	float3 result = dot(f4.rgb, gMonoMult);
	f4.rgb = result.rgb;
    return getUShortRGB_565(f4);
}

/** 二值化閥值 */
float binaryThreshold = 0.2f;
const static float3 yVector = {0.299f, 0.587f, 0.114f};
/** 二值化 - RGBA8888 */
uchar4 __attribute__((kernel)) binary_rgba8888(uchar4 in) {
	float4 f4 = getFloatARGB_8888(in);
	float result = dot(f4.rgb, yVector);
	if (result < binaryThreshold) {
		f4.r = f4.g = f4.b = 0;
	}
	else {
		f4.r = f4.g = f4.b = 1;
	}
    return getUCharARGB_8888(f4);
}
/** 二值化 - RGB565 */
ushort __attribute__((kernel)) binary_rgb565(ushort in) {
	float4 f4 = getFloatRGB_565(in);
	float result = dot(f4.rgb, yVector);
	if (result < binaryThreshold) {
		f4.r = f4.g = f4.b = 0;
	}
	else {
		f4.r = f4.g = f4.b = 1;
	}
    return getUShortRGB_565(f4);
}
