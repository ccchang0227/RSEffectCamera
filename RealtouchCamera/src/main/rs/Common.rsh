
typedef struct {
	float h, s, v;
	float a;
} HSV;

/** ARGB8888[0-255] 轉 符點數RGBA[0-1] */
static float4 getFloatARGB_8888(uchar4 argb8888) {
	float4 f4 = rsUnpackColor8888(argb8888);
	return f4;
}

/** 符點數RGBA[0-1] 轉 ARGB8888[0-255] */
static uchar4 getUCharARGB_8888(float4 argb) {
	uchar4 c4 = rsPackColorTo8888(argb.r, argb.g, argb.b, argb.a);
	return c4;
}

/** RGB565[0-255] 轉 符點數RGBA[0-1] */
static float4 getFloatRGB_565(ushort rgb565) {
	float4 f4;
	uint32_t red = ((rgb565 & 0xF800) >> 11) << 3;
    uint32_t green = ((rgb565 & 0x07E0) >> 5) << 2;
    uint32_t blue = (rgb565 & 0x001F) << 3;
	
	f4.r = red/255.0;
	f4.g = green/255.0;
	f4.b = blue/255.0;
	f4.a = 1.0;
	//rsDebug("src=", rgb565);
	//rsDebug("dest=", f4);
	
	return f4;
}

/** 符點數RGBA[0-1] 轉 RGB565[0-255] */
static ushort getUShortRGB_565(float4 argb) {
	uint32_t red = clamp(argb.r*255, 0, 255);
	uint32_t green = clamp(argb.g*255, 0, 255);
	uint32_t blue = clamp(argb.b*255, 0, 255);
	
	ushort rgb565 = (((red>>3)&0x001F)<<11) | (((green>>2)&0x003F)<<5) | ((blue>>3)&0x001F);
	
	return rgb565;
}

/** 符點數RGBA[0-1] 轉 HSV[0-1] */
static HSV getHSVFrom(float4 rgba) {
	HSV hsv = {0.0, 0.0, 0.0, 0.0};
	hsv.a = rgba.a;
	
	float r = rgba.r;
	float g = rgba.g;
	float b = rgba.b;
	
	float minVal = min(r, min(g, b));
	float maxVal = max(r, max(g, b));
	float diff = maxVal-minVal;
	
	hsv.v = maxVal;
	
	if (maxVal == 0) {
		hsv.s = 0;
	}
	else {
		hsv.s = diff/maxVal;
	}
	
	if (diff == 0) {
		hsv.h = 0;
	}
	else {
		if (r == maxVal && g >= b) {
			hsv.h = (g-b)/(diff*6);
		}
		else if (r == maxVal && g < b) {
			hsv.h = (g-b)/(diff*6) + 1;
		}
		else if (g == maxVal) {
			hsv.h = (b-r)/(diff*6) + 1/3.0;
		}
		else {
			hsv.h = (r-g)/(diff*6) + 2/3.0;
		}
	}
	
	return hsv;
}

/** HSV[0-1] 轉 符點數RGBA[0-1] */
static float4 getRGBAFrom(HSV hsv) {
	float r = 0.0;
	float g = 0.0;
	float b = 0.0;
	float h = hsv.h;
	float s = hsv.s;
	float v = hsv.v;
	
	h = h*6;
	if (h >= 6) {
		h = 0;
	}
	float integer = floor(h);
	float f = h-integer;
	float a = v*(1-s);
	float b1 = v*(1-s*f);
	float c = v*(1-s*(1-f));
	
	if (integer == 0) {
		r = v;
		g = c;
		b = a;
	}
	else if (integer == 1) {
		r = b1;
		g = v;
		b = a;
	}
	else if (integer == 2) {
		r = a;
		g = v;
		b = c;
	}
	else if (integer == 3) {
		r = a;
		g = b1;
		b = v;
	}
	else if (integer == 4) {
		r = c;
		g = a;
		b = v;
	}
	else {
		r = v;
		g = a;
		b = b1;
	}
	
	r = clamp(r, 0.0, 1.0);
	g = clamp(g, 0.0, 1.0);
	b = clamp(b, 0.0, 1.0);
	
	float4 f4;
	f4.a = hsv.a;
	f4.r = r;
	f4.g = g;
	f4.b = b;
	return f4;
}
